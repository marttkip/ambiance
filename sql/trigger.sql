DELIMITER $$
DROP TRIGGER IF EXISTS `t_product_stock_non_pharm_addition_INSERT` $$
CREATE TRIGGER `t_product_stock_non_pharm_addition_INSERT` 
AFTER INSERT ON `product_return_stock`
FOR EACH ROW
BEGIN
    INSERT INTO t_product_stock_non_pharm (transactionId, product_id,category_id,store_id,receiving_store,product_name,store_name,transactionDescription,dr_quantity
																					,cr_quantity,dr_amount,cr_amount,transactionDate,status,product_deleted,transactionCategory,transactionClassification,
																					transactionTable,referenceTable)
    SELECT
					 `product_return_stock`.`product_deductions_stock_id`,
					 `product_return_stock`.`product_id`,
					 `product`.`category_id`,
						product_return_stock.from_store_id,
						product_return_stock.to_store_id,
						product.product_name,
						store.store_name,
						CONCAT('Store Transfer'),
						'0',
						(product_deductions_stock_quantity * product_deductions_stock_pack_size),
						'0',
					 (product.product_unitprice* (product_deductions_stock_quantity * product_deductions_stock_pack_size)),
					 `product_return_stock`.`product_deductions_stock_date`,
					 `product`.`product_status`,
					 `product`.`product_deleted`,
					 'Expense',
					 'Product Addition',
					 'product_return_stock',
					 'product'
		FROM (`product_return_stock`,product,store)
		WHERE product.product_id = product_return_stock.product_id AND product.product_deleted = 0
		AND store.store_id = product_return_stock.from_store_id;

		INSERT INTO t_product_stock_non_pharm (transactionId, product_id,category_id,store_id,receiving_store,product_name,store_name,transactionDescription,dr_quantity
																					,cr_quantity,dr_amount,cr_amount,transactionDate,status,product_deleted,transactionCategory,transactionClassification,
																					transactionTable,referenceTable)
	    SELECT
		       `product_return_stock`.`product_deductions_stock_id`,
		       `product_return_stock`.`product_id`,
		       `product`.`category_id`,
		        product_return_stock.to_store_id,
		        product_return_stock.from_store_id,
		        product.product_name,
		        store.store_name,
		        CONCAT('Store Transfer'),
		        (product_deductions_stock_quantity * product_deductions_stock_pack_size),
		        '0',
		        (product.product_unitprice* (product_deductions_stock_quantity * product_deductions_stock_pack_size)),
		        '0',
		       `product_return_stock`.`product_deductions_stock_date`,
		       `product`.`product_status`,
		       `product`.`product_deleted`,
		       'Income',
		       'Store Deduction',
		       'product_return_stock',
		       'product'
		FROM (`product_return_stock`,product,store)
		WHERE product.product_id = product_return_stock.product_id AND product.product_deleted = 0
		AND store.store_id = product_return_stock.to_store_id;
END $$
DELIMITER ;


DELIMITER $$
DROP TRIGGER IF EXISTS `t_product_stock_non_pharm_addition_UPDATE` $$
CREATE TRIGGER t_product_stock_non_pharm_addition_UPDATE AFTER UPDATE on product_return_stock
FOR EACH ROW
BEGIN
 		UPDATE t_product_stock_non_pharm 
		INNER JOIN product_return_stock ON t_product_stock_non_pharm.transactionId = `product_return_stock`.`product_deductions_stock_id` AND t_product_stock_non_pharm.transactionClassification = 'Product Addition' AND t_product_stock_non_pharm.transactionTable = 'product_return_stock'
		INNER JOIN product ON product.product_id = product_return_stock.product_id AND product.product_deleted = 0
		INNER JOIN store ON store.store_id = product_return_stock.from_store_id
		SET  t_product_stock_non_pharm.transactionId = `product_return_stock`.`product_deductions_stock_id`,
				 t_product_stock_non_pharm.product_id = `product_return_stock`.`product_id`,
				 t_product_stock_non_pharm.category_id = `product`.`category_id`,
				 t_product_stock_non_pharm.store_id = product_return_stock.from_store_id,
				 t_product_stock_non_pharm.receiving_store = product_return_stock.to_store_id,
				 t_product_stock_non_pharm.product_name = product.product_name,
				 t_product_stock_non_pharm.store_name = store.store_name,
				 t_product_stock_non_pharm.transactionDescription = CONCAT('Store Transfer'),
				 t_product_stock_non_pharm.dr_quantity = '0',
				 t_product_stock_non_pharm.cr_quantity = (product_deductions_stock_quantity * product_deductions_stock_pack_size),
				 t_product_stock_non_pharm.dr_amount = '0',
				 t_product_stock_non_pharm.cr_amount = (product.product_unitprice* (product_deductions_stock_quantity * product_deductions_stock_pack_size)),
				 t_product_stock_non_pharm.transactionDate = `product_return_stock`.`product_deductions_stock_date`,
				 t_product_stock_non_pharm.status = `product`.`product_status`,
				 t_product_stock_non_pharm.product_deleted = `product`.`product_deleted`,
				 t_product_stock_non_pharm.transactionCategory = 'Expense',
				 t_product_stock_non_pharm.transactionClassification = 'Product Addition',
				 t_product_stock_non_pharm.transactionTable = 'product_return_stock',
				 t_product_stock_non_pharm.referenceTable = 'product';
		UPDATE t_product_stock_non_pharm 
		INNER JOIN product_return_stock ON t_product_stock_non_pharm.transactionId = `product_return_stock`.`product_deductions_stock_id` 
			AND t_product_stock_non_pharm.transactionClassification = 'Store Deduction' 
			AND t_product_stock_non_pharm.transactionTable = 'product_return_stock'
		INNER JOIN product ON product.product_id = product_return_stock.product_id AND product.product_deleted = 0
		INNER JOIN store ON store.store_id = product_return_stock.to_store_id
		SET  t_product_stock_non_pharm.transactionId = `product_return_stock`.`product_deductions_stock_id`,
				 t_product_stock_non_pharm.product_id = `product_return_stock`.`product_id`,
				 t_product_stock_non_pharm.category_id = `product`.`category_id`,
				 t_product_stock_non_pharm.store_id = product_return_stock.to_store_id,
				 t_product_stock_non_pharm.receiving_store = product_return_stock.from_store_id,
				 t_product_stock_non_pharm.product_name = product.product_name,
				 t_product_stock_non_pharm.store_name = store.store_name,
				 t_product_stock_non_pharm.transactionDescription = CONCAT('Store Transfer'),
				 t_product_stock_non_pharm.dr_quantity = (product_deductions_stock_quantity * product_deductions_stock_pack_size),
				 t_product_stock_non_pharm.cr_quantity = '0',
				 t_product_stock_non_pharm.dr_amount = (product.product_unitprice* (product_deductions_stock_quantity * product_deductions_stock_pack_size)),
				 t_product_stock_non_pharm.cr_amount = '0',
				 t_product_stock_non_pharm.transactionDate = `product_return_stock`.`product_deductions_stock_date`,
				 t_product_stock_non_pharm.status = `product`.`product_status`,
				 t_product_stock_non_pharm.product_deleted = `product`.`product_deleted`,
				 t_product_stock_non_pharm.transactionCategory = 'Income',
				 t_product_stock_non_pharm.transactionClassification = 'Store Deduction',
				 t_product_stock_non_pharm.transactionTable = 'product_return_stock',
				 t_product_stock_non_pharm.referenceTable = 'product';

END $$
DELIMITER ;


-- delete 
DELIMITER $$
DROP TRIGGER IF EXISTS `t_product_stock_non_pharm_addition_DELETE` $$
CREATE TRIGGER t_product_stock_non_pharm_addition_DELETE AFTER DELETE on product_return_stock
FOR EACH ROW
BEGIN
DELETE FROM t_product_stock_non_pharm
    WHERE t_product_stock_non_pharm.transactionId = `product_return_stock`.`product_deductions_stock_id` AND t_product_stock_non_pharm.transactionClassification = 'Product Addition' AND t_product_stock_non_pharm.transactionTable = 'product_return_stock';
DELETE FROM t_product_stock_non_pharm
    WHERE t_product_stock_non_pharm.transactionId = `product_return_stock`.`product_deductions_stock_id` 
    AND t_product_stock_non_pharm.transactionClassification = 'Store Deduction' 
AND t_product_stock_non_pharm.transactionTable = 'product_return_stock';

END $$
DELIMITER ;


-- deduction for product return stock


DELIMITER $$
DROP TRIGGER IF EXISTS `t_product_stock_non_pharm_deduction_INSERT` $$
CREATE TRIGGER `t_product_stock_non_pharm_deduction_INSERT` 
AFTER INSERT ON `product_deductions`
FOR EACH ROW
BEGIN
    INSERT INTO t_product_stock_non_pharm (transactionId, product_id,category_id,store_id,receiving_store,product_name,store_name,transactionDescription,dr_quantity
																					,cr_quantity,dr_amount,cr_amount,transactionDate,status,product_deleted,transactionCategory,transactionClassification,
																					transactionTable,referenceTable)
   SELECT
	  		`product_deductions`.`product_deductions_id`,
	       `product`.`product_id`,
	       `product`.`category_id`,
	       `product_deductions`.`store_id`,
	       `product`.`store_id`,
	       product.product_name,
	       store.store_name,
	       CONCAT('Product Added',' ',`product`.`product_name`),
	        product_deductions.quantity_given,
	   '0' AS `cr_quantity`,
	        (product.product_unitprice * product_deductions.quantity_given),
	        '0',
	       `product_deductions`.`search_date`,
	       `product`.`product_status`,
	       `product`.`product_deleted`,
	       'Income',
	       'Product Addition',
	       'product_deductions',
	       'product'
	FROM (`product_deductions`, `store`, `product`, `orders`)
	WHERE `product_deductions`.`store_id` = store.store_id
	AND product_deductions.quantity_requested > 0
	AND product.product_deleted = 0
	AND product_deductions.product_id = product.product_id
	AND product_deductions.order_id = orders.order_id
	AND orders.order_id = product_deductions.order_id
	AND (orders.is_store = 1 OR orders.is_store = 0)
	AND product_deductions.product_deduction_rejected = 0;

	INSERT INTO t_product_stock_non_pharm (transactionId, product_id,category_id,store_id,receiving_store,product_name,store_name,transactionDescription,dr_quantity
																					,cr_quantity,dr_amount,cr_amount,transactionDate,status,product_deleted,transactionCategory,transactionClassification,
																					transactionTable,referenceTable)
   SELECT
		  `product_deductions`.`product_deductions_id`,
		       `product`.`product_id`,
		       `product`.`category_id`,
		       product.store_id,
		       product_deductions.store_id,
		       product.product_name,
		       store.store_name,
		       CONCAT('Product Deducted',' ',`product`.`product_name`),
		        '0',
		        product_deductions.quantity_given,
		        '0',
		        (product.product_unitprice * product_deductions.quantity_given),
		       `product_deductions`.`search_date`,
		       `product`.`product_status`,
		       `product`.`product_deleted`,
		       'Expense',
		       'Store Deduction',
		       'product_deductions',
		       'product'
		FROM (`product_deductions`, `store`, `product`, `orders`)
		WHERE store.store_id = product_deductions.store_id
		AND product_deductions.quantity_requested > 0
		AND product_deductions.product_id = product.product_id
		AND product_deductions.order_id = orders.order_id
		AND product.product_deleted = 0
		AND (orders.is_store = 1 OR orders.is_store = 0)
		AND product_deductions.product_deduction_rejected = 0;
END $$
DELIMITER;


DELIMITER $$
DROP TRIGGER IF EXISTS `t_product_stock_non_pharm_deduction_UPDATE` $$
CREATE TRIGGER t_product_stock_non_pharm_deduction_UPDATE AFTER UPDATE on product_deductions
FOR EACH ROW
BEGIN
		UPDATE t_product_stock_non_pharm 
		INNER JOIN product_deductions ON t_product_stock_non_pharm.transactionId = `product_deductions`.`product_deductions_id` 
			AND t_product_stock_non_pharm.transactionClassification = 'Product Addition' 
			AND t_product_stock_non_pharm.transactionTable = 'product_deductions' AND product_deductions.product_deduction_rejected = 0
		INNER JOIN product ON product.product_id = product_deductions.product_id AND product.product_deleted = 0
		INNER JOIN store ON store.store_id = product_deductions.store_id
		INNER JOIN orders ON product_deductions.order_id = orders.order_id AND (orders.is_store = 1 OR orders.is_store = 0)
		SET  	 t_product_stock_non_pharm.transactionId = `product_deductions`.`product_deductions_id`,
				 t_product_stock_non_pharm.product_id = `product_deductions`.`product_id`,
				 t_product_stock_non_pharm.category_id = `product`.`category_id`,
				 t_product_stock_non_pharm.store_id = product_deductions.store_id,
				 t_product_stock_non_pharm.receiving_store = product.store_id,
				 t_product_stock_non_pharm.product_name = product.product_name,
				 t_product_stock_non_pharm.store_name = store.store_name,
				 t_product_stock_non_pharm.transactionDescription = CONCAT('Product Deducted',' ',`product`.`product_name`),
				 t_product_stock_non_pharm.dr_quantity = product_deductions.quantity_given,
				 t_product_stock_non_pharm.cr_quantity = '0',
				 t_product_stock_non_pharm.dr_amount = (product.product_unitprice * product_deductions.quantity_given),
				 t_product_stock_non_pharm.cr_amount = '0',
				 t_product_stock_non_pharm.transactionDate = `product_deductions`.`search_date`,
				 t_product_stock_non_pharm.status = `product`.`product_status`,
				 t_product_stock_non_pharm.product_deleted = `product`.`product_deleted`,
				 t_product_stock_non_pharm.transactionCategory = 'Expense',
				 t_product_stock_non_pharm.transactionClassification = 'Store Deduction',
				 t_product_stock_non_pharm.transactionTable = 'product_deductions',
				 t_product_stock_non_pharm.referenceTable = 'product';

 		UPDATE t_product_stock_non_pharm 
		INNER JOIN product_deductions ON t_product_stock_non_pharm.transactionId = `product_deductions`.`product_deductions_id` 
			AND t_product_stock_non_pharm.transactionClassification = 'Store Deduction' 
			AND t_product_stock_non_pharm.transactionTable = 'product_deductions' AND product_deductions.product_deduction_rejected = 0
		INNER JOIN product ON product.product_id = product_deductions.product_id AND product.product_deleted = 0
		INNER JOIN store ON store.store_id = product_deductions.store_id
		INNER JOIN orders ON product_deductions.order_id = orders.order_id AND (orders.is_store = 1 OR orders.is_store = 0)
		SET  	 t_product_stock_non_pharm.transactionId = `product_deductions`.`product_deductions_id`,
				 t_product_stock_non_pharm.product_id = `product_deductions`.`product_id`,
				 t_product_stock_non_pharm.category_id = `product`.`category_id`,
				 t_product_stock_non_pharm.store_id = product.store_id,
				 t_product_stock_non_pharm.receiving_store = product_deductions.store_id,
				 t_product_stock_non_pharm.product_name = product.product_name,
				 t_product_stock_non_pharm.store_name = store.store_name,
				 t_product_stock_non_pharm.transactionDescription = CONCAT('Product Deducted',' ',`product`.`product_name`),
				 t_product_stock_non_pharm.dr_quantity = '0',
				 t_product_stock_non_pharm.cr_quantity = product_deductions.quantity_given,
				 t_product_stock_non_pharm.dr_amount = '0',
				 t_product_stock_non_pharm.cr_amount = (product.product_unitprice * product_deductions.quantity_given),
				 t_product_stock_non_pharm.transactionDate = `product_deductions`.`search_date`,
				 t_product_stock_non_pharm.status = `product`.`product_status`,
				 t_product_stock_non_pharm.product_deleted = `product`.`product_deleted`,
				 t_product_stock_non_pharm.transactionCategory = 'Expense',
				 t_product_stock_non_pharm.transactionClassification = 'Store Deduction',
				 t_product_stock_non_pharm.transactionTable = 'product_deductions',
				 t_product_stock_non_pharm.referenceTable = 'product';
END $$
DELIMITER;


-- DELIMITER $$
-- DROP TRIGGER IF EXISTS `t_product_stock_non_pharm_deduction_DELETE` $$
-- CREATE TRIGGER t_product_stock_non_pharm_deduction_DELETE AFTER DELETE on product_return_stock
-- FOR EACH ROW
-- BEGIN
-- DELETE FROM t_product_stock_non_pharm
--     WHERE t_product_stock_non_pharm.transactionId = `product_return_stock`.`product_deductions_stock_id` 
--     AND t_product_stock_non_pharm.transactionClassification = 'Store Deduction' 
-- AND t_product_stock_non_pharm.transactionTable = 'product_return_stock';
-- END $$
-- DELIMITER;
