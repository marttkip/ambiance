
<section class="panel">
  <header class="panel-heading">
      <h5 class="pull-left"><i class="icon-reorder"></i>Search </h5>
      <div class="clearfix"></div>
  </header>
  <!-- /.box-header -->
  <div class="panel-body">
	<?php
    echo form_open("search-department-expenditure", array("class" => "form-horizontal"));
    ?>
    <div class="row">
      <div class="col-md-2">

        <div class="form-group">
            <label class="col-md-3 control-label">Department: </label>
            <div class="col-md-9">
                <select class="form-control" name="department_id" required>
                  <option value="">---Select an option --- </option>
                  <?php
                  $department_result = '';
                  if($departments->num_rows() > 0)
                  {
                    foreach ($departments->result() as $key => $value) {
                      // code...
                      $department_name = $value->department_name;
                      $department_id = $value->department_id;

                      $department_result .= '<option value="'.$department_id.'"> '.strtoupper($department_name).'</option>';
                    }
                  }
                  echo $department_result;
                  ?>
                </select>
            </div>
        </div>
      </div>
      <div class="col-md-3">

        <div class="form-group">
            <label class="col-md-4 control-label">Account: </label>

            <div class="col-md-8">
              <select class="form-control" name="account_id">
                <option value="">---Select an option --- </option>
                <?php
                $account_result = '';
                if($accounts->num_rows() > 0)
                {
                  foreach ($accounts->result() as $key => $value) {
                    // code...
                    $account_name = $value->account_name;
                    $account_id = $value->account_id;

                    $account_result .= '<option value="'.$account_id.'"> '.strtoupper($account_name).'</option>';
                  }
                }
                echo $account_result;
                ?>
              </select>
            </div>
        </div>



      </div>
        <div class="col-md-3">

          <div class="form-group">
              <label class="col-md-4 control-label">Date From: </label>

              <div class="col-md-8">
                  <div class="input-group">
                      <span class="input-group-addon">
                          <i class="fa fa-calendar"></i>
                      </span>
                      <input data-format="yyyy-MM-dd" type="text" data-plugin-datepicker class="form-control" name="date_from" placeholder=" Date from" value="">
                  </div>
              </div>
          </div>



        </div>
        <div class="col-md-3">
          <div class="form-group">
              <label class="col-md-4 control-label">Date From: </label>

              <div class="col-md-8">
                  <div class="input-group">
                      <span class="input-group-addon">
                          <i class="fa fa-calendar"></i>
                      </span>
                      <input data-format="yyyy-MM-dd" type="text" data-plugin-datepicker class="form-control" name="date_to" placeholder=" Date to" value="">
                  </div>
              </div>
          </div>

        </div>
        <div class="col-md-1">
        	 <div class="form-group">
                <div class="col-lg-8 col-lg-offset-4">
                	<div class="center-align">
                   		<button type="submit" class="btn btn-info">Search</button>
    				</div>
                </div>
            </div>
        </div>
    </div>


    <?php
    echo form_close();
    ?>
  </div>
</section>
