<?php   if ( ! defined('BASEPATH')) exit('No direct script access allowed');
require_once "./application/modules/admin/controllers/admin.php";
error_reporting(E_ALL);
class Reconcilliation extends admin
{
    var $documents_path;


	function __construct()
	{
		parent:: __construct();
		$this->load->model('auth/auth_model');
		$this->load->model('financials/financials_model');
	    $this->load->model('admin/admin_model');
	    $this->load->model('admin/users_model');
	    $this->load->model('site/site_model');
	    $this->load->model('financials/company_financial_model');
	    $this->load->model('reception/database');
	    $this->load->model('financials/ledgers_model');
	     $this->load->model('financials/reconcilliation_model');



	    $this->load->model('admin/file_model');

		//path to image directory
		$this->documents_path = realpath(APPPATH . '../assets/documents/vehicles');


		$this->load->library('image_lib');

		if(!$this->auth_model->check_login())
		{
			redirect('login');
		}
	}


	public function bank()
	{
		


		$data['title'] = 'Bank Reconcilliation';
		$v_data['title'] = $data['title'];
		$data['content'] = $this->load->view('financials/reconcilliation/accounts', $v_data, true);
	    $this->load->view('admin/templates/general_page', $data);
	}
	public function add_new_recon()
	{
		$data['accounts'] = $this->reconcilliation_model->get_child_accounts("Bank");
		$data['all_accounts'] = $this->reconcilliation_model->get_all_accounts();

		
		$page = $this->load->view('financials/reconcilliation/add_new_recon',$data,true);
		// var_dump($page);die();
		echo $page;
	}
	public function get_account_reconcilliation($account_id)
	{
		$this->db->where('recon_status = 1 AND account_id ='.$account_id);
		$this->db->order_by('recon_date','DESC');
		$this->db->limit(1);
		$query = $this->db->get('bank_reconcilliation');


		if($query->num_rows() > 0)
		{
			foreach ($query->result() as $key => $value) {
				# code...
				$start_date = $value->recon_date;
				$account_opening_balance = $value->opening_balance;
				$ending_balance = $value->ending_balance;
				$service_charged = $value->service_charged;
				$interest_earned = $value->interest_earned;
				$expense_account_id = $value->expense_account_id;
				$charged_date = $value->charged_date;
				$interest_date = $value->interest_date;
				$interest_account_id = $value->interest_account_id;
			}
		}
		else
		{

			$this->db->where('account_id ='.$account_id);
			$this->db->limit(1);
			$query = $this->db->get('account');
			if($query->num_rows() > 0)
			{
				foreach ($query->result() as $key => $value2) {
					# code...
					$start_date = $value2->start_date;
					$account_opening_balance = $value2->account_opening_balance;
					$ending_balance = 0;
					$service_charged = 0;
					$interest_earned = 0;
					$expense_account_id = 0;
					$charged_date = $value2->start_date;
					$interest_date = $value2->start_date;
					$interest_account_id = 0;
			
				}
			}
		}


		$response['message'] ='success';
		$response['start_date'] = $start_date;
		$response['account_opening_balance'] = $account_opening_balance;
		$response['ending_balance'] = $ending_balance;
		$response['service_charged'] = $service_charged;
		$response['interest_earned'] = $interest_earned;
		$response['expense_account_id'] = $expense_account_id;
		$response['interest_account_id'] = $interest_account_id;
		$response['interest_date'] = $interest_date;
		$response['charged_date'] = $charged_date;

		echo json_encode($response);
	}


	public function add_reconcilliation()
	{
		$this->form_validation->set_rules('account_id', 'From','required|xss_clean');
		
		if ($this->form_validation->run())
		{
			//update order
			if($this->reconcilliation_model->add_reconcilliation())
			{

				
				$response['message'] ='success';
				$response['result'] ='You have successfully updated the payment';

			}
			
			else
			{
				// $this->session->set_userdata('error_message', 'Could not Direct Purchases. Please try again');
				$response['message'] ='fail';
				$response['result'] ='Sorry could not update this payment detail';
			}
		}
		else
		{
			// $this->session->set_userdata('error_message', validation_errors());	

			$response['message'] ='fail';
			$response['result'] = strip_tags(validation_errors());
		}

		echo json_encode($response);
	}

	public function view_recon_details($recon_id)
	{
		$v_data['recon_id'] = $recon_id;
		// var_dump($recon_id);die();
		$data['title'] = 'Account Name Reconcilliation';
		$v_data['title'] = $data['title'];
		$data['content'] = $this->load->view('financials/reconcilliation/recon_view', $v_data, true);
	    $this->load->view('admin/templates/general_page', $data);
	}

	public function get_money_out($recon_id)
	{
		$data['recon_id'] = $recon_id;


		$page = $this->load->view('financials/reconcilliation/money_out',$data,true);
		// var_dump($page);die();
		echo $page;
	}

	public function get_money_in($recon_id)
	{
		$data['recon_id'] = $recon_id;


		$page = $this->load->view('financials/reconcilliation/money_in',$data,true);
		// var_dump($page);die();
		echo $page;
	}


	public function get_total_recons($recon_id)
	{
		$total_money_out = $this->reconcilliation_model->get_money_out_total($recon_id);
		$total_money_in = $this->reconcilliation_model->get_money_in_total($recon_id);

		$this->db->where('recon_id  ='.$recon_id);
		$this->db->order_by('recon_date','DESC');
		$this->db->limit(1);
		$query = $this->db->get('bank_reconcilliation');


		if($query->num_rows() > 0)
		{
			foreach ($query->result() as $key => $value) {
				# code...
				$start_date = $value->recon_date;
				$account_opening_balance = $value->opening_balance;
				$ending_balance = $value->ending_balance;
				$service_charged = $value->service_charged;
				$interest_earned = $value->interest_earned;
				$expense_account_id = $value->expense_account_id;
				$charged_date = $value->charged_date;
				$interest_date = $value->interest_date;
				$interest_account_id = $value->interest_account_id;
				$ending_balance = $value->ending_balance;
			}
		}

		$cleared_balance = ($total_money_in + $interest_earned) - ($total_money_out - $service_charged);
		$difference = $ending_balance - $cleared_balance;
		$response['service_charged'] = $service_charged;
		$response['interest_earned'] = $interest_earned;
		$response['money_in'] = $total_money_in;
		$response['money_out'] = $total_money_out;
		$response['ending_balance'] = $ending_balance;
		$response['cleared_balance'] = $cleared_balance;
		$response['difference'] = $difference;
		$response['total_money_in'] = number_format($total_money_in,2);
		$response['total_money_out'] = number_format($total_money_out,2);
		$response['total_service_charged'] = number_format($service_charged,2);
		$response['total_interest_earned'] = number_format($interest_earned,2);
		$response['total_ending_balance'] = number_format($ending_balance,2);
		$response['total_cleared_balance'] = number_format($cleared_balance,2);
		$response['total_difference'] = number_format($difference,2);



		echo json_encode($response);
	}

	public function update_transaction($transaction_id,$recon_id,$type)
	{
		if($type == 1)
		{
			// creditor

			// check if it exisits
			$this->db->where('recon_id = 0 AND creditor_payment_id ='.$transaction_id);
			$query = $this->db->get('creditor_payment');

			if($query->num_rows() > 0)
			{
				$update_recon['recon_id'] = $recon_id;
				$this->db->where('creditor_payment_id',$transaction_id);
				$this->db->update('creditor_payment',$update_recon);
			}
			else
			{
				$update_recon['recon_id'] = 0;
				$this->db->where('creditor_payment_id',$transaction_id);
				$this->db->update('creditor_payment',$update_recon);
			}

			
		}
		else if($type == 2)
		{
			$this->db->where('recon_id = 0 AND account_payment_id ='.$transaction_id);
			$query = $this->db->get('account_payments');

			if($query->num_rows() > 0)
			{
				// direct purchase
				$update_account['recon_id'] = $recon_id;
				$this->db->where('account_payment_id',$transaction_id);
				$this->db->update('account_payments',$update_account);
			}
			else
			{
				$update_account['recon_id'] = 0;
				$this->db->where('account_payment_id',$transaction_id);
				$this->db->update('account_payments',$update_account);

			}
		}
		else if($type == 3)
		{
			// transfers

			$this->db->where('recon_id = 0 AND finance_transfer_id ='.$transaction_id);
			$query = $this->db->get('finance_transfer');

			if($query->num_rows() > 0)
			{

				$update_transfer['recon_id'] = $recon_id;
				$this->db->where('finance_transfer_id',$transaction_id);
				$this->db->update('finance_transfer',$update_transfer);
			}
			else
			{
				$update_transfer['recon_id'] = 0;
				$this->db->where('finance_transfer_id',$transaction_id);
				$this->db->update('finance_transfer',$update_transfer);
			}
		}

	}

	public function update_transaction_in($transaction_id,$recon_id,$type)
	{
		if($type == 1)
		{
			// creditor

			// check if it exisits
			$this->db->where('recon_id = 0 AND finance_transfered_id ='.$transaction_id);
			$query = $this->db->get('finance_transfered');

			if($query->num_rows() > 0)
			{

				$update_transfer['recon_id'] = $recon_id;
				$this->db->where('finance_transfered_id',$transaction_id);
				$this->db->update('finance_transfered',$update_transfer);
			}
			else
			{
				$update_transfer['recon_id'] = 0;
				$this->db->where('finance_transfered_id',$transaction_id);
				$this->db->update('finance_transfered',$update_transfer);
			}

			
		}
		else if($type == 2)
		{
			$this->db->where('recon_id = 0 AND journal_entry_id ='.$transaction_id);
			$query = $this->db->get('journal_entry');

			if($query->num_rows() > 0)
			{
				// direct purchase
				$update_account['recon_id'] = $recon_id;
				$this->db->where('journal_entry_id',$transaction_id);
				$this->db->update('journal_entry',$update_account);
			}
			else
			{
				$update_account['recon_id'] = 0;
				$this->db->where('journal_entry_id',$transaction_id);
				$this->db->update('journal_entry',$update_account);

			}
		}
		else if($type == 3)
		{
			
		}
	}

	public function complete_reconcilliation($recon_id)
	{
		$ending_balance = $this->input->post('ending_balance');
		$cleared_balance = $this->input->post('cleared_balance');
		$difference = $this->input->post('difference');
		$total_cheques = $this->input->post('total_cheques');
		$total_deposits = $this->input->post('total_deposits');

		
		$update_recon['recon_status'] = 2;
		$update_recon['difference'] = $difference;
		$update_recon['money_out'] = $total_cheques;
		$update_recon['money_in'] = $total_deposits;
		$update_recon['cleared_balance'] = $cleared_balance;
		$update_recon['ending_balance'] = $ending_balance;
		$this->db->where('recon_id',$recon_id);
		if($this->db->update('bank_reconcilliation',$update_recon))
		{
			$response['message'] = 'success';
			$response['result'] = 'You have successfully completed this reconcilliation';
		}
		else
		{
			$response['message'] = 'success';
			$response['result'] = 'Sorry something went wrong. Please try again';
		}

		echo json_encode($response);
	}

	public function print_recon_details($recon_id)
	{

		$v_data['recon_id'] = $recon_id;
		$v_data['contacts'] = $this->site_model->get_contacts();
		$v_data['search_title'] = 'Reconcilliations';
		$v_data['title'] = 'Reconcilliations';
		$this->load->view('financials/reconcilliation/print_recon', $v_data);

	}



}
?>