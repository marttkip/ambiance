<div class="row">
	<div class="col-md-12">
		
		<section class="panel">
			<header class="panel-heading">
				<h2 class="panel-title">Lab Tests</h2>
			</header>

			<div class="panel-body">
				<?php echo "
				<div class='center-align'>
					<input name='close' type='button' value='Close' class='btn btn-primary' onclick='close_window(".$visit_id.")' />
				</div>";
				?>
                <div class="row" style="padding:30px 0 30px 0;">
                    <div class="col-xs-8">
                        <select id='lab_test_id' name='lab_test_id' class="form-control custom-select">
                            <option value=''>None - Please Select a lab test</option>
                            <?php echo $lab;?>
                        </select>
                    </div>
                    <div class="col-xs-4">
                        <button type='submit' class="btn btn-sm btn-success"  onclick="pass_lab_test(<?php echo $visit_id;?>);"> Add Test</button>
                    </div>
                </div>
				<?php echo "
				<div class='center-align'>
					<input name='close' type='button' value='Close' class='btn btn-primary' onclick='close_window(".$visit_id.")' />
				</div>";
				?>   
            </div>
        </section>
    </div>
</div>
<?php echo form_close();?>
<script type="text/javascript">
$(document).ready(function(){

    $("#lab_test_id").customselect();
});
	
	function close_window(visit_id)
	{
		window.close(this);
	}

	function pass_lab_test(visit_id)
	{
		var lab_test_id = document.getElementById("lab_test_id").value;
		save_lab_test(lab_test_id, visit_id);
	}
	
	function save_lab_test(val, visit_id)
	{
		var config_url = $('#config_url').val();
		var nav_link = config_url+"nurse/save_lab_test/"+val+"/"+visit_id;
		$.get(nav_link, function( data ) 
		{
			if(data != "")
			{
				alert(data);
			}
			else
			{
				alert('Test added successfully');
				get_tests(visit_id);
			}
		});
	}
	
	function get_tests(visit_id)
	{
		var config_url = $('#config_url').val();
		var url = config_url+"laboratory/test_lab/"+visit_id;
		$.get(url, function( data ) 
		{
			$("#lab_table", window.opener.document).html(data);
			get_lab_results(visit_id);
		});
	}
	
	function get_lab_results(visit_id)
	{
		var config_url = $('#config_url').val();
		var nav_link = config_url+"medical_record/get_lab_test_results/"+visit_id;
		$.get(nav_link, function( data ) 
		{
			$("#lab_test_results_section", window.opener.document).html(data);
		});
	}
</script>