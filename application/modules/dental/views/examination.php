<?php

	$patient_health_form = $this->dental_model->get_patient_history_forms(3,0,7);

	$health =  "<table class='table table-condensed table-striped table-bordered table-hover'> ";
	$count_disease = 0;

	if($patient_health_form->num_rows() > 0)
	{
		$patient_health_rs = $patient_health_form->result();
		
		foreach($patient_health_rs as $dis)
		{	
			$fd_id = $dis->patient_history_id;
			$disease = $dis->patient_history_name;
			
			

			$query_result = $this->dental_model->get_patient_history_results($fd_id,$patient_id,$visit_id);

			if($query_result->num_rows() > 0)
			{
				$patient_history_result = $query_result->result();
				foreach ($patient_history_result as $key) {
					# code...
					$patient_history_status = $key->patient_history_status;
				}
			}
			else
			{
				$patient_history_status = 0;
			}

			if($patient_history_status == 1)
			{
				$checked = "checked='checked'";
				$checked_value = 0;
			}
			else
			{
				$checked = '';
				$checked_value = 1;
			}
							
			$health =  $health."<tr>
										<td style='width:90%' >".$disease." </td>
										<td style='width:10%'> 
											<input type='checkbox' id='checkbox".$fd_id.$patient_id."' onclick='update_patient_history(".$fd_id.",".$patient_id.", ".$visit_id.")' ".$checked." value='".$checked_value."'>
										</td>  
								</tr>";
			
			$count_disease++;
		}
	}
	$health .= "</table>
				";





$history2 =  "<div class='row'> ";



$patient_history_form1 = $this->dental_model->get_patient_history_forms(1,0,7);


// var_dump($patient_history_form1); die();
$count_disease = 0;

if($patient_history_form1->num_rows() > 0)
{
	$patient_history_rs = $patient_history_form1->result();
	$checked_one = '';
	$checked_two = '';
	$checked_three = '';
	$checked_four = '';
	$checked_five = '';

	$checked_value_one = 1;
	$checked_value_two = 2;
	$checked_value_three = 3;
	$checked_value_four = 4;
	$checked_value_five = 5;
	foreach($patient_history_rs as $dis)
	{	
		$fd_id = $dis->patient_history_id;
		$disease = $dis->patient_history_name;
		
		

		$query_result = $this->dental_model->get_patient_history_results($fd_id,$patient_id,$visit_id);

		if($query_result->num_rows() > 0)
		{
			$patient_history_result = $query_result->result();
			foreach ($patient_history_result as $key) {
				# code...
				$patient_history_status = $key->patient_history_status;
				$item_id = $key->item_id;


				if($patient_history_status == 1 AND $item_id == 1)
				{
					$checked_one = "checked='checked'";
					$checked_value_one = 1;
				}
				else if($patient_history_status == 1 AND $item_id == 2)
				{
					$checked_two = "checked='checked'";
					$checked_value_two = 2;
				}
				
			}
		}
		

		// echo $checked_three;
		
						
		$history2 =  $history2."<div class='row'>
									<div class='col-md-4'>
										<strong>".$disease."</strong>
									</div>
									<div class='col-md-4'>
										<input type='checkbox' id='checkbox".$fd_id.$patient_id."' onclick='update_patient_history_changes(".$fd_id.",".$patient_id.", ".$visit_id.",1)' ".$checked_one." value='".$checked_value_one."'> CONVEX 
									</div>
									<div class='col-md-4'> 
										<input type='checkbox' id='checkbox".$fd_id.$patient_id."' onclick='update_patient_history_changes(".$fd_id.",".$patient_id.", ".$visit_id.",2)' ".$checked_two." value='".$checked_value_two."'> CONCAVE 
									</div>
									
								</div>";
		
		$count_disease++;
	}
}

$patient_history_form2 = $this->dental_model->get_patient_history_forms(2,0,7);

if($patient_history_form2->num_rows() > 0)
{
	$patient_history_rs = $patient_history_form2->result();
	$checked_one = '';
	$checked_two = '';
	$checked_three = '';
	$checked_four = '';
	$checked_five = '';

	$checked_value_one = 1;
	$checked_value_two = 2;
	$checked_value_three = 3;
	$checked_value_four = 4;
	$checked_value_five = 5;
	foreach($patient_history_rs as $dis)
	{	
		$fd_id = $dis->patient_history_id;
		$disease = $dis->patient_history_name;
		
		

		$query_result = $this->dental_model->get_patient_history_results($fd_id,$patient_id,$visit_id);

		if($query_result->num_rows() > 0)
		{
			$patient_history_result = $query_result->result();
			foreach ($patient_history_result as $key) {
				# code...
				$patient_history_status = $key->patient_history_status;
				$item_id = $key->item_id;


				if($patient_history_status == 1 AND $item_id == 1)
				{
					$checked_one = "checked='checked'";
					$checked_value_one = 1;
				}
				else if($patient_history_status == 1 AND $item_id == 2)
				{
					$checked_two = "checked='checked'";
					$checked_value_two = 2;
				}
				
			}
		}
		
		$v_data['query'] = $this->dental_model->get_dental_notes(1, $visit_id);
		$mobile_personnel_id = $this->session->userdata('personnel_id');
		if(!isset($mobile_personnel_id))
		{
			$mobile_personnel_id = NULL;
		}
		$v_data['mobile_personnel_id'] = $mobile_personnel_id;
		$v_data['visit_id'] = $visit_id;

		$notes = $this->load->view('nurse/patients/notes', $v_data, TRUE);
		$result = '';
		$result .= '<div class="row">
		      	<div class="col-md-12">
		  			<div class="form-group">
		   				<label class="col-lg-12 control-label">DETAILS :  </label>
			    		<div class="col-lg-12">
			      				<textarea id="doctors_oral_health_notes" rows="5" cols="50"  class="form-control col-md-12 cleditor" > </textarea>
			      		</div>
			      	</div>
			    </div>

			  </div>';
		$result .=  '
		  <br>
			<div class="row">
		        <div class="form-group">
		            <div class="col-lg-12">
		                <div class="center-align">
		                      <a hred="#" class="btn btn-sm btn-info" onclick="save_oral_health_notes('.$visit_id.','.$patient_id.',1)">Save Note</a>
		                     
		                  </div>
		            </div>
		        </div>
		    </div>';

		// echo $checked_three;
		
		$history2 =  $history2."<hr><div class='row'>
									<div class='col-md-4'>
										<strong>".$disease."</strong>
									</div>
									<div class='col-md-4'>
										<input type='checkbox' id='checkbox".$fd_id.$patient_id."' onclick='update_patient_history_changes(".$fd_id.",".$patient_id.", ".$visit_id.",1)' ".$checked_one." value='".$checked_value_one."'> YES 
									</div>
									<div class='col-md-4'> 
										<input type='checkbox' id='checkbox".$fd_id.$patient_id."' onclick='update_patient_history_changes(".$fd_id.",".$patient_id.", ".$visit_id.",2)' ".$checked_two." value='".$checked_value_two."'> NO 
									</div>
									<div class='col-md-12'> 
										<div id='facial-asymmetry-description'></div>
									</div>
									
								</div>";		
		
		
		$count_disease++;
	}
}


$patient_history_form3 = $this->dental_model->get_patient_history_forms(3,0,7);

if($patient_history_form3->num_rows() > 0)
{
	$patient_history_rs = $patient_history_form3->result();
	$checked_one = '';
	$checked_two = '';
	$checked_three = '';
	$checked_four = '';
	$checked_five = '';

	$checked_value_one = 1;
	$checked_value_two = 2;
	$checked_value_three = 3;
	$checked_value_four = 4;
	$checked_value_five = 5;
	foreach($patient_history_rs as $dis)
	{	
		$fd_id = $dis->patient_history_id;
		$disease = $dis->patient_history_name;
		
		

		$query_result = $this->dental_model->get_patient_history_results($fd_id,$patient_id,$visit_id);

		if($query_result->num_rows() > 0)
		{
			$patient_history_result = $query_result->result();
			foreach ($patient_history_result as $key) {
				# code...
				$patient_history_status = $key->patient_history_status;
				$item_id = $key->item_id;


				if($patient_history_status == 1 AND $item_id == 1)
				{
					$checked_one = "checked='checked'";
					$checked_value_one = 1;
				}
				else if($patient_history_status == 1 AND $item_id == 2)
				{
					$checked_two = "checked='checked'";
					$checked_value_two = 2;
				}
				
			}
		}
		

		// echo $checked_three;
		
		$history2 =  $history2."<hr><div class='row'>
									<div class='col-md-4'>
										<strong>".$disease."</strong>
									</div>
									<div class='col-md-4'>
										<input type='checkbox' id='checkbox".$fd_id.$patient_id."' onclick='update_patient_history_changes(".$fd_id.",".$patient_id.", ".$visit_id.",1)' ".$checked_one." value='".$checked_value_one."'> YES 
									</div>
									<div class='col-md-4'> 
										<input type='checkbox' id='checkbox".$fd_id.$patient_id."' onclick='update_patient_history_changes(".$fd_id.",".$patient_id.", ".$visit_id.",2)' ".$checked_two." value='".$checked_value_two."'> NO 
									</div>
									
									
								</div>";	
						
		
		
		$count_disease++;
	}
}

$history2 .= "</div>
			";













?>
<div class="row">
	<div class="col-md-12">		
		<div class="col-md-6">

			<div class='row'>
				<table class='table table-condensed table-striped table-bordered table-hover'>
					<tr>
						<td colspan='4'><h4 class='center-align'>EXTRA - ORAL EXAM</h4></td>
					</tr>
				</table>
			</div>
			<!-- <h4>EXTRA-ORAL EXAM</h4> -->
			<div class="padd" style="margin-top: 5px;">
				<div class="col-md-12">
					<?php echo $history2;?>
				</div>
				
			</div>
		</div>
		<div class="col-md-6">
			<div class='row'>
				<table class='table table-condensed table-striped table-bordered table-hover'>
					<tr>
						<td colspan='4'><h4 class='center-align'>INTRA - ORAL EXAM</h4></td>
					</tr>
				</table>
			</div>
			<div class="padd" style="margin-top: 5px;">
				<div class="col-md-12">
					<div id="oral-teeth"></div>

					<div id="intra-oral-description"></div>
					
				</div>
			</div>
		</div>
	</div>
</div>
<br>

<?php
$patient_health_form4 = $this->dental_model->get_patient_history_forms(4,0,7);

$health7 =  "";
$count_disease = 0;

if($patient_health_form4->num_rows() > 0)
{
	$patient_health_rs = $patient_health_form4->result();
	$checked_one = '';
	$checked_two = '';
	$checked_three = '';
	$checked_four = '';
	$checked_five = '';

	$checked_value_one = 1;
	$checked_value_two = 2;
	$checked_value_three = 3;
	$checked_value_four = 4;
	$checked_value_five = 5;
	foreach($patient_health_rs as $dis)
	{	
		$fd_id = $dis->patient_history_id;
		$disease = $dis->patient_history_name;
		
		

		$query_result = $this->dental_model->get_patient_history_results($fd_id,$patient_id,$visit_id);

		if($query_result->num_rows() > 0)
		{
			$patient_history_result = $query_result->result();
			foreach ($patient_history_result as $key) {
				# code...
				$patient_history_status = $key->patient_history_status;
				$item_id = $key->item_id;

				if($patient_history_status == 1 AND $item_id == 1)
				{
					$checked_one = "checked='checked'";
					$checked_value_one = 1;
				}
				else if($patient_history_status == 1 AND $item_id == 2)
				{
					$checked_two = "checked='checked'";
					$checked_value_two = 2;
				}
				

			}
		}
		
		$health7 =  $health7."
								<div class='row'>
									
									<div class='col-md-6'>
										CLASS I <input type='checkbox' id='checkbox".$fd_id.$patient_id."' onclick='update_patient_history_changes(".$fd_id.",".$patient_id.", ".$visit_id.",1)' ".$checked_one." value='".$checked_value_one."'> 
									</div>
									<div class='col-md-6'> 
										TYPE <input type='checkbox' id='checkbox".$fd_id.$patient_id."' onclick='update_patient_history_changes(".$fd_id.",".$patient_id.", ".$visit_id.",2)' ".$checked_two." value='".$checked_value_two."'> 
									</div>
									
									
								</div><hr>";					
		
		
		$count_disease++;
	}
}

$patient_health_form5 = $this->dental_model->get_patient_history_forms(5,0,7);

$count_disease = 0;

if($patient_health_form5->num_rows() > 0)
{
	$patient_health_rs = $patient_health_form5->result();
	$checked_one = '';
	$checked_two = '';
	$checked_three = '';
	$checked_four = '';
	$checked_five = '';

	$checked_value_one = 1;
	$checked_value_two = 2;
	$checked_value_three = 3;
	$checked_value_four = 4;
	$checked_value_five = 5;
	foreach($patient_health_rs as $dis)
	{	
		$fd_id = $dis->patient_history_id;
		$disease = $dis->patient_history_name;
		
		

		$query_result = $this->dental_model->get_patient_history_results($fd_id,$patient_id,$visit_id);

		if($query_result->num_rows() > 0)
		{
			$patient_history_result = $query_result->result();
			foreach ($patient_history_result as $key) {
				# code...
				$patient_history_status = $key->patient_history_status;
				$item_id = $key->item_id;

				if($patient_history_status == 1 AND $item_id == 1)
				{
					$checked_one = "checked='checked'";
					$checked_value_one = 1;
				}
				else if($patient_history_status == 1 AND $item_id == 2)
				{
					$checked_two = "checked='checked'";
					$checked_value_two = 2;
				}
				else if($patient_history_status == 1 AND $item_id == 3)
				{
					$checked_three = "checked='checked'";
					$checked_value_three = 3;
				}
				

			}
		}
		
		$health7 =  $health7."
								<div class='row'>
									
									<div class='col-md-6'>
										CLASS I <input type='checkbox' id='checkbox".$fd_id.$patient_id."' onclick='update_patient_history_changes(".$fd_id.",".$patient_id.", ".$visit_id.",1)' ".$checked_one." value='".$checked_value_one."'> 
									</div>
									<div class='col-md-6'> 
										DIV I <input type='checkbox' id='checkbox".$fd_id.$patient_id."' onclick='update_patient_history_changes(".$fd_id.",".$patient_id.", ".$visit_id.",2)' ".$checked_two." value='".$checked_value_two."'> 
										<br>
										<br>
										DIV II <input type='checkbox' id='checkbox".$fd_id.$patient_id."' onclick='update_patient_history_changes(".$fd_id.",".$patient_id.", ".$visit_id.",3)' ".$checked_three." value='".$checked_value_three."'> 
									</div>
									
									
								</div><hr>";					
		
		
		$count_disease++;
	}
}


$patient_health_form6 = $this->dental_model->get_patient_history_forms(6,0,7);

$count_disease = 0;

if($patient_health_form6->num_rows() > 0)
{
	$patient_health_rs = $patient_health_form6->result();
	$checked_one = '';
	$checked_two = '';
	$checked_three = '';
	$checked_four = '';
	$checked_five = '';

	$checked_value_one = 1;
	$checked_value_two = 2;
	$checked_value_three = 3;
	$checked_value_four = 4;
	$checked_value_five = 5;
	foreach($patient_health_rs as $dis)
	{	
		$fd_id = $dis->patient_history_id;
		$disease = $dis->patient_history_name;
		
		

		$query_result = $this->dental_model->get_patient_history_results($fd_id,$patient_id,$visit_id);

		if($query_result->num_rows() > 0)
		{
			$patient_history_result = $query_result->result();
			foreach ($patient_history_result as $key) {
				# code...
				$patient_history_status = $key->patient_history_status;
				$item_id = $key->item_id;

				if($patient_history_status == 1 AND $item_id == 1)
				{
					$checked_one = "checked='checked'";
					$checked_value_one = 1;
				}
				else if($patient_history_status == 1 AND $item_id == 2)
				{
					$checked_two = "checked='checked'";
					$checked_value_two = 2;
				}
				else if($patient_history_status == 1 AND $item_id == 3)
				{
					$checked_three = "checked='checked'";
					$checked_value_three = 3;
				}
				

			}
		}
		
		$health7 =  $health7."
								<div class='row'>
									
									<div class='col-md-6'>
										CLASS I <input type='checkbox' id='checkbox".$fd_id.$patient_id."' onclick='update_patient_history_changes(".$fd_id.",".$patient_id.", ".$visit_id.",1)' ".$checked_one." value='".$checked_value_one."'> 
									</div>
									<div class='col-md-6'> 
										TYPE <input type='checkbox' id='checkbox".$fd_id.$patient_id."' onclick='update_patient_history_changes(".$fd_id.",".$patient_id.", ".$visit_id.",2)' ".$checked_two." value='".$checked_value_two."'> 
										
									</div>
									
									
								</div><hr>";					
		
		
		$count_disease++;
	}
}


$patient_health_form6 = $this->dental_model->get_patient_history_forms(6,0,7);

$count_disease = 0;

if($patient_health_form6->num_rows() > 0)
{
	$patient_health_rs = $patient_health_form6->result();
	$checked_one = '';
	$checked_two = '';
	$checked_three = '';
	$checked_four = '';
	$checked_five = '';

	$checked_value_one = 1;
	$checked_value_two = 2;
	$checked_value_three = 3;
	$checked_value_four = 4;
	$checked_value_five = 5;
	foreach($patient_health_rs as $dis)
	{	
		$fd_id = $dis->patient_history_id;
		$disease = $dis->patient_history_name;
		
		

		$query_result = $this->dental_model->get_patient_history_results($fd_id,$patient_id,$visit_id);

		if($query_result->num_rows() > 0)
		{
			$patient_history_result = $query_result->result();
			foreach ($patient_history_result as $key) {
				# code...
				$patient_history_status = $key->patient_history_status;
				$item_id = $key->item_id;

				if($patient_history_status == 1 AND $item_id == 1)
				{
					$checked_one = "checked='checked'";
					$checked_value_one = 1;
				}
				else if($patient_history_status == 1 AND $item_id == 2)
				{
					$checked_two = "checked='checked'";
					$checked_value_two = 2;
				}
				else if($patient_history_status == 1 AND $item_id == 3)
				{
					$checked_three = "checked='checked'";
					$checked_value_three = 3;
				}
				

			}
		}
		
		$health7 =  $health7."
								<div class='row'>
									
									<div class='col-md-6'>
										CLASS I <input type='checkbox' id='checkbox".$fd_id.$patient_id."' onclick='update_patient_history_changes(".$fd_id.",".$patient_id.", ".$visit_id.",1)' ".$checked_one." value='".$checked_value_one."'> 
									</div>
									<div class='col-md-6'> 
										TYPE <input type='checkbox' id='checkbox".$fd_id.$patient_id."' onclick='update_patient_history_changes(".$fd_id.",".$patient_id.", ".$visit_id.",2)' ".$checked_two." value='".$checked_value_two."'> 
										
									</div>
									
									
								</div>";					
		
		
		$count_disease++;
	}
}
?>
<div class="row">
	<div class="col-md-12">		
		<div class="col-md-4">
			<div class='row'>
				<table class='table table-condensed table-striped table-bordered table-hover'>
					<tr>
						<td colspan='4'><h4 class='center-align'>INCISOR RELATIONSHIP</h4></td>
					</tr>
				</table>
			</div>
			<!-- <h4>EXTRA-ORAL EXAM</h4> -->
			<div class="padd" style="margin-top: 5px;">
				<div class="col-md-12">
					<?php echo $health7;?>
				</div>
				<div class='form-group'>
									
					<div class='col-md-4'>
						OVERJECT 
					</div>
					<div class='col-md-7'>
						<input type='text' id='checkbox' class="form-control"  value=''>  
					</div>
					<div class='col-md-1'>
						mm
					</div>
					
					
					
				</div>
				<div class='form-group'>		
					<div class='col-md-4'>
						OVERBITE 
					</div>
					<div class='col-md-7'>
						<input type='text' id='checkbox' class="form-control"  value=''>  
					</div>
					<div class='col-md-1'>
						%
					</div>
					
				</div>


				<hr>
				
			</div>
		</div>
		<?php
			$patient_health_form7 = $this->dental_model->get_patient_history_forms(7,0,7);

			$count_disease = 0;
			$health8 ='';
			if($patient_health_form7->num_rows() > 0)
			{
				$patient_health_rs = $patient_health_form7->result();
				$checked_one = '';
				$checked_two = '';
				$checked_three = '';
				$checked_four = '';
				$checked_five = '';
				$checked_six = '';
				$checked_seven = '';
				$checked_eight = '';
				$checked_nine = '';
				$checked_ten = '';
				$checked_eleven = '';
				$checked_twelve = '';

				$checked_value_one = 1;
				$checked_value_two = 2;
				$checked_value_three = 3;
				$checked_value_four = 4;
				$checked_value_five = 5;
				$checked_value_six = 6;
				$checked_value_seven = 7;
				$checked_value_eight = 8;
				$checked_value_nine = 9;
				$checked_value_ten = 10;
				$checked_value_eleven = 11;
				$checked_value_twelve = 12;
				foreach($patient_health_rs as $dis)
				{	
					$fd_id = $dis->patient_history_id;
					$disease = $dis->patient_history_name;
					
					

					$query_result = $this->dental_model->get_patient_history_results($fd_id,$patient_id,$visit_id);

					if($query_result->num_rows() > 0)
					{
						$patient_history_result = $query_result->result();
						foreach ($patient_history_result as $key) {
							# code...
							$patient_history_status = $key->patient_history_status;
							$item_id = $key->item_id;

							if($patient_history_status == 1 AND $item_id == 1)
							{
								$checked_one = "checked='checked'";
								$checked_value_one = 1;
							}
							else if($patient_history_status == 1 AND $item_id == 2)
							{
								$checked_two = "checked='checked'";
								$checked_value_two = 2;
							}
							else if($patient_history_status == 1 AND $item_id == 3)
							{
								$checked_three = "checked='checked'";
								$checked_value_three = 3;
							}
							else if($patient_history_status == 1 AND $item_id == 4)
							{
								$checked_four = "checked='checked'";
								$checked_value_four = 4;
							}
							else if($patient_history_status == 1 AND $item_id == 5)
							{
								$checked_five = "checked='checked'";
								$checked_value_five = 5;
							}
							else if($patient_history_status == 1 AND $item_id == 6)
							{
								$checked_six = "checked='checked'";
								$checked_value_six = 6;
							}
							else if($patient_history_status == 1 AND $item_id == 7)
							{
								$checked_seven = "checked='checked'";
								$checked_value_seven = 7;
							}
							else if($patient_history_status == 1 AND $item_id == 8)
							{
								$checked_eight = "checked='checked'";
								$checked_value_eight = 8;
							}
							else if($patient_history_status == 1 AND $item_id == 9)
							{
								$checked_nine = "checked='checked'";
								$checked_value_nine = 9;
							}
							else if($patient_history_status == 1 AND $item_id == 10)
							{
								$checked_ten = "checked='checked'";
								$checked_value_ten = 10;
							}
							else if($patient_history_status == 1 AND $item_id == 11)
							{
								$checked_eleven = "checked='checked'";
								$checked_value_eleven = 11;
							}
							else if($patient_history_status == 1 AND $item_id == 12)
							{
								$checked_twelve = "checked='checked'";
								$checked_value_twelve = 12;
							}

						}
					}
					
					$health8 =  $health8."
											<div class='row'>
												<div class='col-md-2'>
													CLASS I
												</div>
												<div class='col-md-5'>
													RIGHT <input type='checkbox' id='checkbox".$fd_id.$patient_id."' onclick='update_patient_history_changes(".$fd_id.",".$patient_id.", ".$visit_id.",1)' ".$checked_one." value='".$checked_value_one."'> 
												</div>
												<div class='col-md-5'> 
													LEFT <input type='checkbox' id='checkbox".$fd_id.$patient_id."' onclick='update_patient_history_changes(".$fd_id.",".$patient_id.", ".$visit_id.",2)' ".$checked_two." value='".$checked_value_two."'> 
													
												</div>
												
												
											</div>
											<hr>
											<div class='row'>
												<div class='col-md-2'>
													CLASS II RIGHT
												</div>

												<div class='col-md-2'>
													 <input type='checkbox' id='checkbox".$fd_id.$patient_id."' onclick='update_patient_history_changes(".$fd_id.",".$patient_id.", ".$visit_id.",3)' ".$checked_three." value='".$checked_value_three."'> RIGHT
												</div>
												<div class='col-md-2'>
													 <input type='checkbox' id='checkbox".$fd_id.$patient_id."' onclick='update_patient_history_changes(".$fd_id.",".$patient_id.", ".$visit_id.",4)' ".$checked_four." value='".$checked_value_four."'> 1/4
												</div>
												<div class='col-md-2'>
													 <input type='checkbox' id='checkbox".$fd_id.$patient_id."' onclick='update_patient_history_changes(".$fd_id.",".$patient_id.", ".$visit_id.",5)' ".$checked_five." value='".$checked_value_five."'> 1/2
												</div>
												<div class='col-md-2'>
													 <input type='checkbox' id='checkbox".$fd_id.$patient_id."' onclick='update_patient_history_changes(".$fd_id.",".$patient_id.", ".$visit_id.",6)' ".$checked_six." value='".$checked_value_six."'> 3/4
												</div>
												<div class='col-md-2'>
													 <input type='checkbox' id='checkbox".$fd_id.$patient_id."' onclick='update_patient_history_changes(".$fd_id.",".$patient_id.", ".$visit_id.",7)' ".$checked_seven." value='".$checked_value_seven."'> FULL
												</div>
												
												
												
											</div>


											<hr>
											<div class='row'>
												<div class='col-md-2'>
													CLASS II LEFT
												</div>
												<div class='col-md-2'>
													 <input type='checkbox' id='checkbox".$fd_id.$patient_id."' onclick='update_patient_history_changes(".$fd_id.",".$patient_id.", ".$visit_id.",8)' ".$checked_eight." value='".$checked_value_eight."'> RIGHT
												</div>
												<div class='col-md-2'>
													 <input type='checkbox' id='checkbox".$fd_id.$patient_id."' onclick='update_patient_history_changes(".$fd_id.",".$patient_id.", ".$visit_id.",9)' ".$checked_nine." value='".$checked_value_nine."'> 1/4
												</div>
												<div class='col-md-2'>
													 <input type='checkbox' id='checkbox".$fd_id.$patient_id."' onclick='update_patient_history_changes(".$fd_id.",".$patient_id.", ".$visit_id.",10)' ".$checked_ten." value='".$checked_value_ten."'> 1/2
												</div>
												<div class='col-md-2'>
													 <input type='checkbox' id='checkbox".$fd_id.$patient_id."' onclick='update_patient_history_changes(".$fd_id.",".$patient_id.", ".$visit_id.",11)' ".$checked_eleven." value='".$checked_value_eleven."'> 3/4
												</div>
												<div class='col-md-2'>
													 <input type='checkbox' id='checkbox".$fd_id.$patient_id."' onclick='update_patient_history_changes(".$fd_id.",".$patient_id.", ".$visit_id.",12)' ".$checked_twelve." value='".$checked_value_twelve."'> FULL
												</div>
												
												
												
											</div>

											";					
					
					
					$count_disease++;
				}
			}
		?>
		<div class="col-md-4">
			<div class='row'>
				<table class='table table-condensed table-striped table-bordered table-hover'>
					<tr>
						<td colspan='4'><h4 class='center-align'>MOLAR RELATIONSHIP</h4></td>
					</tr>
				</table>
			</div>
			<!-- <h4>EXTRA-ORAL EXAM</h4> -->
			<div class="padd" style="margin-top: 5px;">
				<div class="col-md-12">
					<?php echo $health8;?>
				</div>
				
			</div>

			
		</div>

		<?php
			$patient_health_form8 = $this->dental_model->get_patient_history_forms(8,0,7);

			$count_disease = 0;
			$health9 ='';
			if($patient_health_form8->num_rows() > 0)
			{
				$patient_health_rs = $patient_health_form8->result();
				$checked_one = '';
				$checked_two = '';
				$checked_three = '';
				$checked_four = '';
				$checked_five = '';
				$checked_six = '';
				$checked_seven = '';
				$checked_eight = '';
				$checked_nine = '';
				$checked_ten = '';
				$checked_eleven = '';
				$checked_twelve = '';

				$checked_value_one = 1;
				$checked_value_two = 2;
				$checked_value_three = 3;
				$checked_value_four = 4;
				$checked_value_five = 5;
				$checked_value_six = 6;
				$checked_value_seven = 7;
				$checked_value_eight = 8;
				$checked_value_nine = 9;
				$checked_value_ten = 10;
				$checked_value_eleven = 11;
				$checked_value_twelve = 12;
				foreach($patient_health_rs as $dis)
				{	
					$fd_id = $dis->patient_history_id;
					$disease = $dis->patient_history_name;
					
					

					$query_result = $this->dental_model->get_patient_history_results($fd_id,$patient_id,$visit_id);

					if($query_result->num_rows() > 0)
					{
						$patient_history_result = $query_result->result();
						foreach ($patient_history_result as $key) {
							# code...
							$patient_history_status = $key->patient_history_status;
							$item_id = $key->item_id;

							if($patient_history_status == 1 AND $item_id == 1)
							{
								$checked_one = "checked='checked'";
								$checked_value_one = 1;
							}
							else if($patient_history_status == 1 AND $item_id == 2)
							{
								$checked_two = "checked='checked'";
								$checked_value_two = 2;
							}
							else if($patient_history_status == 1 AND $item_id == 3)
							{
								$checked_three = "checked='checked'";
								$checked_value_three = 3;
							}
							else if($patient_history_status == 1 AND $item_id == 4)
							{
								$checked_four = "checked='checked'";
								$checked_value_four = 4;
							}
							else if($patient_history_status == 1 AND $item_id == 5)
							{
								$checked_five = "checked='checked'";
								$checked_value_five = 5;
							}
							else if($patient_history_status == 1 AND $item_id == 6)
							{
								$checked_six = "checked='checked'";
								$checked_value_six = 6;
							}
							else if($patient_history_status == 1 AND $item_id == 7)
							{
								$checked_seven = "checked='checked'";
								$checked_value_seven = 7;
							}
							else if($patient_history_status == 1 AND $item_id == 8)
							{
								$checked_eight = "checked='checked'";
								$checked_value_eight = 8;
							}
							else if($patient_history_status == 1 AND $item_id == 9)
							{
								$checked_nine = "checked='checked'";
								$checked_value_nine = 9;
							}
							else if($patient_history_status == 1 AND $item_id == 10)
							{
								$checked_ten = "checked='checked'";
								$checked_value_ten = 10;
							}
							else if($patient_history_status == 1 AND $item_id == 11)
							{
								$checked_eleven = "checked='checked'";
								$checked_value_eleven = 11;
							}
							else if($patient_history_status == 1 AND $item_id == 12)
							{
								$checked_twelve = "checked='checked'";
								$checked_value_twelve = 12;
							}

						}
					}
					
					$health9 =  $health9."
											<div class='row'>
												<div class='col-md-2'>
													CLASS I
												</div>
												<div class='col-md-5'>
													RIGHT <input type='checkbox' id='checkbox".$fd_id.$patient_id."' onclick='update_patient_history_changes(".$fd_id.",".$patient_id.", ".$visit_id.",1)' ".$checked_one." value='".$checked_value_one."'> 
												</div>
												<div class='col-md-5'> 
													LEFT <input type='checkbox' id='checkbox".$fd_id.$patient_id."' onclick='update_patient_history_changes(".$fd_id.",".$patient_id.", ".$visit_id.",2)' ".$checked_two." value='".$checked_value_two."'> 
													
												</div>
												
												
											</div>
											<hr>
											<div class='row'>
												<div class='col-md-2'>
													CLASS II RIGHT
												</div>

												<div class='col-md-2'>
													 <input type='checkbox' id='checkbox".$fd_id.$patient_id."' onclick='update_patient_history_changes(".$fd_id.",".$patient_id.", ".$visit_id.",3)' ".$checked_three." value='".$checked_value_three."'> RIGHT
												</div>
												<div class='col-md-2'>
													 <input type='checkbox' id='checkbox".$fd_id.$patient_id."' onclick='update_patient_history_changes(".$fd_id.",".$patient_id.", ".$visit_id.",4)' ".$checked_four." value='".$checked_value_four."'> 1/4
												</div>
												<div class='col-md-2'>
													 <input type='checkbox' id='checkbox".$fd_id.$patient_id."' onclick='update_patient_history_changes(".$fd_id.",".$patient_id.", ".$visit_id.",5)' ".$checked_five." value='".$checked_value_five."'> 1/2
												</div>
												<div class='col-md-2'>
													 <input type='checkbox' id='checkbox".$fd_id.$patient_id."' onclick='update_patient_history_changes(".$fd_id.",".$patient_id.", ".$visit_id.",6)' ".$checked_six." value='".$checked_value_six."'> 3/4
												</div>
												<div class='col-md-2'>
													 <input type='checkbox' id='checkbox".$fd_id.$patient_id."' onclick='update_patient_history_changes(".$fd_id.",".$patient_id.", ".$visit_id.",7)' ".$checked_seven." value='".$checked_value_seven."'> FULL
												</div>
												
												
												
											</div>


											<hr>
											<div class='row'>
												<div class='col-md-2'>
													CLASS II LEFT
												</div>
												<div class='col-md-2'>
													 <input type='checkbox' id='checkbox".$fd_id.$patient_id."' onclick='update_patient_history_changes(".$fd_id.",".$patient_id.", ".$visit_id.",8)' ".$checked_eight." value='".$checked_value_eight."'> RIGHT
												</div>
												<div class='col-md-2'>
													 <input type='checkbox' id='checkbox".$fd_id.$patient_id."' onclick='update_patient_history_changes(".$fd_id.",".$patient_id.", ".$visit_id.",9)' ".$checked_nine." value='".$checked_value_nine."'> 1/4
												</div>
												<div class='col-md-2'>
													 <input type='checkbox' id='checkbox".$fd_id.$patient_id."' onclick='update_patient_history_changes(".$fd_id.",".$patient_id.", ".$visit_id.",10)' ".$checked_ten." value='".$checked_value_ten."'> 1/2
												</div>
												<div class='col-md-2'>
													 <input type='checkbox' id='checkbox".$fd_id.$patient_id."' onclick='update_patient_history_changes(".$fd_id.",".$patient_id.", ".$visit_id.",11)' ".$checked_eleven." value='".$checked_value_eleven."'> 3/4
												</div>
												<div class='col-md-2'>
													 <input type='checkbox' id='checkbox".$fd_id.$patient_id."' onclick='update_patient_history_changes(".$fd_id.",".$patient_id.", ".$visit_id.",12)' ".$checked_twelve." value='".$checked_value_twelve."'> FULL
												</div>
												
												
												
											</div>

											";					
					
					
					$count_disease++;
				}
			}
		?>

		<div class="col-md-4">
			<div class='row'>
				<table class='table table-condensed table-striped table-bordered table-hover'>
					<tr>
						<td colspan='4'><h4 class='center-align'>CANINE RELATIONSHIP</h4></td>
					</tr>
				</table>
			</div>
			<!-- <h4>EXTRA-ORAL EXAM</h4> -->
			<div class="padd" style="margin-top: 5px;">
				<div class="col-md-12">
					<?php echo $health9;?>
				</div>
				
			</div>


		</div>
	</div>
</div>

<?php
$patient_health_form9 = $this->dental_model->get_patient_history_forms(9,0,7);

$count_disease = 0;
$health10= '';
if($patient_health_form9->num_rows() > 0)
{
	$patient_health_rs = $patient_health_form9->result();
	$checked_one = '';
	$checked_two = '';
	$checked_three = '';
	$checked_four = '';
	$checked_five = '';

	$checked_value_one = 1;
	$checked_value_two = 2;
	$checked_value_three = 3;
	$checked_value_four = 4;
	$checked_value_five = 5;
	foreach($patient_health_rs as $dis)
	{	
		$fd_id = $dis->patient_history_id;
		$disease = $dis->patient_history_name;
		
		

		$query_result = $this->dental_model->get_patient_history_results($fd_id,$patient_id,$visit_id);

		if($query_result->num_rows() > 0)
		{
			$patient_history_result = $query_result->result();
			foreach ($patient_history_result as $key) {
				# code...
				$patient_history_status = $key->patient_history_status;
				$item_id = $key->item_id;

				if($patient_history_status == 1 AND $item_id == 1)
				{
					$checked_one = "checked='checked'";
					$checked_value_one = 1;
				}
				else if($patient_history_status == 1 AND $item_id == 2)
				{
					$checked_two = "checked='checked'";
					$checked_value_two = 2;
				}
				else if($patient_history_status == 1 AND $item_id == 3)
				{
					$checked_three = "checked='checked'";
					$checked_value_three = 3;
				}
				

			}
		}
		
		$health10 =  $health10."
								<div class='row'>
									
									<div class='col-md-12'>
										 <input type='checkbox' id='checkbox".$fd_id.$patient_id."' onclick='update_patient_history_changes(".$fd_id.",".$patient_id.", ".$visit_id.",1)' ".$checked_one." value='".$checked_value_one."'> YES
									</div>
									<div class='col-md-12'> 
										 <input type='checkbox' id='checkbox".$fd_id.$patient_id."' onclick='update_patient_history_changes(".$fd_id.",".$patient_id.", ".$visit_id.",2)' ".$checked_two." value='".$checked_value_two."'> NO
										
									</div>
									
									
								</div>";					
		
		
		$count_disease++;
	}
}
?>
<div class="row">	
	<div class="col-md-12">	
		<div class='row'>
			<table class='table table-condensed table-striped table-bordered table-hover'>
				<tr>
					<td colspan='4'><h4 class='left-align'>CROSSIBET</h4></td>
				</tr>
			</table>
		</div>
		<div class="col-md-12" style="margin-top: 5px;">	
			<div class="col-md-6">
				<?php echo $health10;?>
			</div>
			<div class="col-md-6">
				<div id="crossibet-description"></div>
				
			</div>
			
		</div>
	</div>
</div>
<br>
<div class="row">	
	<div class="col-md-6">	
		<div class='row'>
			<table class='table table-condensed table-striped table-bordered table-hover'>
				<tr>
					<td colspan='4'><h4 class='left-align'>ROTATION</h4></td>
				</tr>
			</table>
		</div>
		<div class="col-md-12" style="margin-top: 5px;">	
			
				<div id="rotation-teeth"></div>
		
			
			
		</div>
	</div>
	
	<div class="col-md-6">	
		<div class='row'>
			<table class='table table-condensed table-striped table-bordered table-hover'>
				<tr>
					<td colspan='4'><h4 class='left-align'>SPACING</h4></td>
				</tr>
			</table>
		</div>
		<div class="col-md-12" style="margin-top: 5px;">	
			<div class="col-md-12">
				<div class="form-group">
                    <label class="col-md-4 control-label">UPPER ARCH: </label>
                    
                    <div class="col-md-8">
                        <input type="text" class="form-control" name="upper_arch" placeholder="" value="<?php echo set_value('upper_arch');?>">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-4 control-label">LOWER ARCH: </label>
                    
                    <div class="col-md-8">
                        <input type="text" class="form-control" name="lower_arch" placeholder="" value="<?php echo set_value('lower_arch');?>">
                    </div>
                </div>
			</div>
			
			
		</div>
	</div>
</div>
<?php
$patient_health_form10 = $this->dental_model->get_patient_history_forms(10,0,7);

$count_disease = 0;
$health11= '';
if($patient_health_form10->num_rows() > 0)
{
	$patient_health_rs = $patient_health_form10->result();
	$checked_one = '';
	$checked_two = '';
	$checked_three = '';
	$checked_four = '';
	$checked_five = '';

	$checked_value_one = 1;
	$checked_value_two = 2;
	$checked_value_three = 3;
	$checked_value_four = 4;
	$checked_value_five = 5;
	foreach($patient_health_rs as $dis)
	{	
		$fd_id = $dis->patient_history_id;
		$disease = $dis->patient_history_name;
		
		

		$query_result = $this->dental_model->get_patient_history_results($fd_id,$patient_id,$visit_id);

		if($query_result->num_rows() > 0)
		{
			$patient_history_result = $query_result->result();
			foreach ($patient_history_result as $key) {
				# code...
				$patient_history_status = $key->patient_history_status;
				$item_id = $key->item_id;

				if($patient_history_status == 1 AND $item_id == 1)
				{
					$checked_one = "checked='checked'";
					$checked_value_one = 1;
				}
				else if($patient_history_status == 1 AND $item_id == 2)
				{
					$checked_two = "checked='checked'";
					$checked_value_two = 2;
				}
				else if($patient_history_status == 1 AND $item_id == 3)
				{
					$checked_three = "checked='checked'";
					$checked_value_three = 3;
				}
				

			}
		}
		
		$health11 =  $health11."
								<div class='row'>
									
									<div class='col-md-4'>
										 THIN <input type='checkbox' id='checkbox".$fd_id.$patient_id."' onclick='update_patient_history_changes(".$fd_id.",".$patient_id.", ".$visit_id.",1)' ".$checked_one." value='".$checked_value_one."'> 
									</div>
									<div class='col-md-4'> 
										 NORMAL <input type='checkbox' id='checkbox".$fd_id.$patient_id."' onclick='update_patient_history_changes(".$fd_id.",".$patient_id.", ".$visit_id.",2)' ".$checked_two." value='".$checked_value_two."'> 
										
									</div>
									<div class='col-md-4'> 
										 THICK <input type='checkbox' id='checkbox".$fd_id.$patient_id."' onclick='update_patient_history_changes(".$fd_id.",".$patient_id.", ".$visit_id.",3)' ".$checked_three." value='".$checked_value_three."'> 
										
									</div>
									
									
								</div>";					
		
		
		$count_disease++;
	}
}
?>
<br>
<div class="row">	
	<div class="col-md-12">	
		<div class='row'>
			<table class='table table-condensed table-striped table-bordered table-hover'>
				<tr>
					<td colspan='4'><h4 class='left-align'>SOFT TISSUE BIOTYPE</h4></td>
				</tr>
			</table>
		</div>
		<div class="col-md-12" style="margin-top: 5px;">	
			<div class="col-md-6">
				<?php echo $health11?>
			</div>
			
			
		</div>
	</div>
</div>

<div class="row">	
	<div class="col-md-12">	
		<div class='row'>
			<table class='table table-condensed table-striped table-bordered table-hover'>
				<tr>
					<td colspan='4'><h4 class='left-align'>RADIOGRAPH</h4></td>
				</tr>
			</table>
		</div>
		<div class="col-md-12" style="margin-top: 5px;">	
			<div class="col-md-6">
				<div id="opg-description"></div>
				
			</div>
			<div class="col-md-6">
				<div id="ceph-description"></div>
				
			</div>
			
			<div class="col-md-6">
				<div id="pa-description"></div>
				
			</div>
			<div class="col-md-6">
				<div id="bw-description"></div>
				
			</div>
			<div class="col-md-12">
				<div id="diagnosis-description"></div>
				
			
			</div>
			
			
		</div>
	</div>
</div>
<br>
<div class="row">	
	<div class="col-md-12">	
		<div class='row'>
			<table class='table table-condensed table-striped table-bordered table-hover'>
				<tr>
					<td colspan='4'><h4 class='left-align'>PROBLEM LIST</h4></td>
				</tr>
			</table>
		</div>
		<div class="col-md-12" style="margin-top: 5px;">
			<div id="problem-description"></div>	
			
			
		</div>
	</div>
</div>
<div class="row">	
	<div class="col-md-12">	
		<div class='row'>
			<table class='table table-condensed table-striped table-bordered table-hover'>
				<tr>
					<td colspan='4'><h4 class='left-align'>TREATMENT PLAN</h4></td>
				</tr>
			</table>
		</div>
		<div class="col-md-12" style="margin-top: 5px;">
			<div id="treatment-description"></div>	
			
			
		</div>
	</div>
</div>
<br>
<div class="row" style="margin-top: 5px;">	
	<div class="col-md-12">	
		<div class='row'>
			<table class='table table-condensed table-striped table-bordered table-hover'>
				<tr>
					<td colspan='4'><h4 class='left-align'>MECHANO THERAPY</h4></td>
				</tr>
			</table>
		</div>
		<div class="col-md-6" >	
			<div id="anchorage-description"></div>
			
			
		</div>
		<div class="col-md-6" >	
			<div id="appliance-description"></div>
			
			
		</div>
	</div>
</div>
<br>
<?php
$patient_health_form10 = $this->dental_model->get_patient_history_forms(10,0,7);

$count_disease = 0;
$health12= '';
if($patient_health_form10->num_rows() > 0)
{
	$patient_health_rs = $patient_health_form10->result();
	$checked_one = '';
	$checked_two = '';
	$checked_three = '';
	$checked_four = '';
	$checked_five = '';
	$checked_six = '';

	$checked_value_one = 1;
	$checked_value_two = 2;
	$checked_value_three = 3;
	$checked_value_four = 4;
	$checked_value_five = 5;
	$checked_value_six = 6;
	foreach($patient_health_rs as $dis)
	{	
		$fd_id = $dis->patient_history_id;
		$disease = $dis->patient_history_name;
		
		

		$query_result = $this->dental_model->get_patient_history_results($fd_id,$patient_id,$visit_id);

		if($query_result->num_rows() > 0)
		{
			$patient_history_result = $query_result->result();
			foreach ($patient_history_result as $key) {
				# code...
				$patient_history_status = $key->patient_history_status;
				$item_id = $key->item_id;

				if($patient_history_status == 1 AND $item_id == 1)
				{
					$checked_one = "checked='checked'";
					$checked_value_one = 1;
				}
				else if($patient_history_status == 1 AND $item_id == 2)
				{
					$checked_two = "checked='checked'";
					$checked_value_two = 2;
				}
				else if($patient_history_status == 1 AND $item_id == 3)
				{
					$checked_three = "checked='checked'";
					$checked_value_three = 3;
				}
				else if($patient_history_status == 1 AND $item_id == 4)
				{
					$checked_four = "checked='checked'";
					$checked_value_four = 4;
				}
				else if($patient_history_status == 1 AND $item_id == 5)
				{
					$checked_five = "checked='checked'";
					$checked_value_five = 5;
				}
				else if($patient_history_status == 1 AND $item_id == 6)
				{
					$checked_six = "checked='checked'";
					$checked_value_six = 6;
				}
				else if($patient_history_status == 1 AND $item_id == 7)
				{
					$checked_seven = "checked='checked'";
					$checked_value_seven = 7;
				}
				else if($patient_history_status == 1 AND $item_id == 8)
				{
					$checked_eight = "checked='checked'";
					$checked_value_eight = 8;
				}
				else if($patient_history_status == 1 AND $item_id == 9)
				{
					$checked_nine = "checked='checked'";
					$checked_value_nine = 9;
				}
				else if($patient_history_status == 1 AND $item_id == 10)
				{
					$checked_ten = "checked='checked'";
					$checked_value_ten = 10;
				}
				else if($patient_history_status == 1 AND $item_id == 11)
				{
					$checked_eleven = "checked='checked'";
					$checked_value_eleven = 11;
				}
				else if($patient_history_status == 1 AND $item_id == 12)
				{
					$checked_twelve = "checked='checked'";
					$checked_value_twelve = 12;
				}
				

			}
		}
		
		$health12 =  $health12."
								<div class='row'>
									
									<div class='col-md-4'>
										  <input type='checkbox' id='checkbox".$fd_id.$patient_id."' onclick='update_patient_history_changes(".$fd_id.",".$patient_id.", ".$visit_id.",1)' ".$checked_one." value='".$checked_value_one."'> BANDS
									</div>
									<div class='col-md-4'> 
										  <input type='checkbox' id='checkbox".$fd_id.$patient_id."' onclick='update_patient_history_changes(".$fd_id.",".$patient_id.", ".$visit_id.",2)' ".$checked_two." value='".$checked_value_two."'> 16
										
									</div>
									<div class='col-md-4'> 
										  <input type='checkbox' id='checkbox".$fd_id.$patient_id."' onclick='update_patient_history_changes(".$fd_id.",".$patient_id.", ".$visit_id.",3)' ".$checked_three." value='".$checked_value_three."'> 26
										
									</div>



									
									
								</div>
								<div class='row'>
									
									<div class='col-md-4'>
										  <input type='checkbox' id='checkbox".$fd_id.$patient_id."' onclick='update_patient_history_changes(".$fd_id.",".$patient_id.", ".$visit_id.",4)' ".$checked_four." value='".$checked_value_four."'> BUCCAL TUBES
									</div>
									<div class='col-md-4'> 
										  <input type='checkbox' id='checkbox".$fd_id.$patient_id."' onclick='update_patient_history_changes(".$fd_id.",".$patient_id.", ".$visit_id.",2)' ".$checked_five." value='".$checked_value_five."'> 46
										
									</div>
									<div class='col-md-4'> 
										  <input type='checkbox' id='checkbox".$fd_id.$patient_id."' onclick='update_patient_history_changes(".$fd_id.",".$patient_id.", ".$visit_id.",6)' ".$checked_six." value='".$checked_value_six."'> 36
										
									</div>


									
									
									
								</div>";					
		
		
		$count_disease++;
	}
}
$patient_health_form11 = $this->dental_model->get_patient_history_forms(12,0,7);

$count_disease = 0;
$health13= '';
if($patient_health_form11->num_rows() > 0)
{
	$patient_health_rs = $patient_health_form11->result();
	$checked_one = '';
	$checked_two = '';
	$checked_three = '';
	$checked_four = '';
	$checked_five = '';

	$checked_value_one = 1;
	$checked_value_two = 2;
	$checked_value_three = 3;
	$checked_value_four = 4;
	$checked_value_five = 5;
	foreach($patient_health_rs as $dis)
	{	
		$fd_id = $dis->patient_history_id;
		$disease = $dis->patient_history_name;
		
		

		$query_result = $this->dental_model->get_patient_history_results($fd_id,$patient_id,$visit_id);

		if($query_result->num_rows() > 0)
		{
			$patient_history_result = $query_result->result();
			foreach ($patient_history_result as $key) {
				# code...
				$patient_history_status = $key->patient_history_status;
				$item_id = $key->item_id;

				if($patient_history_status == 1 AND $item_id == 1)
				{
					$checked_one = "checked='checked'";
					$checked_value_one = 1;
				}
				else if($patient_history_status == 1 AND $item_id == 2)
				{
					$checked_two = "checked='checked'";
					$checked_value_two = 2;
				}
				else if($patient_history_status == 1 AND $item_id == 3)
				{
					$checked_three = "checked='checked'";
					$checked_value_three = 3;
				}
				else if($patient_history_status == 1 AND $item_id == 4)
				{
					$checked_four = "checked='checked'";
					$checked_value_four = 4;
				}
				else if($patient_history_status == 1 AND $item_id == 5)
				{
					$checked_five = "checked='checked'";
					$checked_value_five = 5;
				}
				

			}
		}
		
		$health13 =  $health13."
								<div class='row'>
									
									<div class='col-md-4'>
										  ROTH <input type='checkbox' id='checkbox".$fd_id.$patient_id."' onclick='update_patient_history_changes(".$fd_id.",".$patient_id.", ".$visit_id.",1)' ".$checked_one." value='".$checked_value_one."'> 
									</div>
									<div class='col-md-4'> 
										  MBT <input type='checkbox' id='checkbox".$fd_id.$patient_id."' onclick='update_patient_history_changes(".$fd_id.",".$patient_id.", ".$visit_id.",2)' ".$checked_two." value='".$checked_value_two."'> 
										
									</div>
									<div class='col-md-4'> 
										 OTHER <input type='checkbox' id='checkbox".$fd_id.$patient_id."' onclick='update_patient_history_changes(".$fd_id.",".$patient_id.", ".$visit_id.",3)' ".$checked_three." value='".$checked_value_three."'> 
										
									</div>



									
									
								</div>
								<div class='row'>
									
									<div class='col-md-6'>
										  <input type='checkbox' id='checkbox".$fd_id.$patient_id."' onclick='update_patient_history_changes(".$fd_id.",".$patient_id.", ".$visit_id.",4)' ".$checked_four." value='".$checked_value_four."'> 22
									</div>
									<div class='col-md-6'> 
										  <input type='checkbox' id='checkbox".$fd_id.$patient_id."' onclick='update_patient_history_changes(".$fd_id.",".$patient_id.", ".$visit_id.",5)' ".$checked_five." value='".$checked_value_five."'> 18
										
									</div>
									


									
									
									
								</div>";					
		
		
		$count_disease++;
	}
}
?>
<div class="row" style="margin-top: 5px;">	
	<div class="col-md-6">	
		<div class='row'>
			<table class='table table-condensed table-striped table-bordered table-hover'>
				<tr>
					<td colspan='4'><h4 class='left-align'>STRAP UP</h4></td>
				</tr>
			</table>
		</div>
		<div class='padd'>
			<?php echo $health12;?>
		</div>
	</div>
	<div class="col-md-6">	
		<div class='row'>
			<table class='table table-condensed table-striped table-bordered table-hover'>
				<tr>
					<td colspan='4'><h4 class='left-align'>BRACKET SLOT</h4></td>
				</tr>
			</table>
		</div>
		<div class='padd'>
			<?php echo $health13;?>
		</div>
	</div>
</div>
<br>
<?php

$patient_health_form12 = $this->dental_model->get_patient_history_forms(13,0,7);

$count_disease = 0;
$health14= '';
if($patient_health_form12->num_rows() > 0)
{
	$patient_health_rs = $patient_health_form12->result();
	$checked_one = '';
	$checked_two = '';
	$checked_three = '';
	$checked_four = '';
	$checked_five = '';
	$checked_six = '';
	$checked_seven = '';
	$checked_eight = '';
	$checked_nine = '';
	$checked_ten = '';
	$checked_eleven = '';
	$checked_twelve = '';

	$checked_value_one = 1;
	$checked_value_two = 2;
	$checked_value_three = 3;
	$checked_value_four = 4;
	$checked_value_five = 5;
	$checked_value_six = 6;
	$checked_value_seven = 7;
	$checked_value_eight = 8;
	$checked_value_nine = 9;
	$checked_value_ten = 10;
	$checked_value_eleven = 11;
	$checked_value_twelve = 12;
	foreach($patient_health_rs as $dis)
	{	
		$fd_id = $dis->patient_history_id;
		$disease = $dis->patient_history_name;
		
		

		$query_result = $this->dental_model->get_patient_history_results($fd_id,$patient_id,$visit_id);

		if($query_result->num_rows() > 0)
		{
			$patient_history_result = $query_result->result();
			foreach ($patient_history_result as $key) {
				# code...
				$patient_history_status = $key->patient_history_status;
				$item_id = $key->item_id;

				if($patient_history_status == 1 AND $item_id == 1)
				{
					$checked_one = "checked='checked'";
					$checked_value_one = 1;
				}
				else if($patient_history_status == 1 AND $item_id == 2)
				{
					$checked_two = "checked='checked'";
					$checked_value_two = 2;
				}
				else if($patient_history_status == 1 AND $item_id == 3)
				{
					$checked_three = "checked='checked'";
					$checked_value_three = 3;
				}
				else if($patient_history_status == 1 AND $item_id == 4)
				{
					$checked_four = "checked='checked'";
					$checked_value_four = 4;
				}
				else if($patient_history_status == 1 AND $item_id == 5)
				{
					$checked_five = "checked='checked'";
					$checked_value_five = 5;
				}
				else if($patient_history_status == 1 AND $item_id == 6)
				{
					$checked_six = "checked='checked'";
					$checked_value_six = 6;
				}
				else if($patient_history_status == 1 AND $item_id == 7)
				{
					$checked_seven = "checked='checked'";
					$checked_value_seven = 7;
				}
				else if($patient_history_status == 1 AND $item_id == 8)
				{
					$checked_eight = "checked='checked'";
					$checked_value_eight = 8;
				}
				else if($patient_history_status == 1 AND $item_id == 9)
				{
					$checked_nine = "checked='checked'";
					$checked_value_nine = 9;
				}
				else if($patient_history_status == 1 AND $item_id == 10)
				{
					$checked_ten = "checked='checked'";
					$checked_value_ten = 10;
				}
				else if($patient_history_status == 1 AND $item_id == 11)
				{
					$checked_eleven = "checked='checked'";
					$checked_value_eleven = 11;
				}
				else if($patient_history_status == 1 AND $item_id == 12)
				{
					$checked_twelve = "checked='checked'";
					$checked_value_twelve = 12;
				}

			}
		}
		
		$health14 =  $health14."
								<div class='row'>
									<div class='col-md-1'>
									NIII UPPER
									</div>
									<div class='col-md-2'>
										  012 <input type='checkbox' id='checkbox".$fd_id.$patient_id."' onclick='update_patient_history_changes(".$fd_id.",".$patient_id.", ".$visit_id.",1)' ".$checked_one." value='".$checked_value_one."'> 
									</div>
									<div class='col-md-3'>
										  014 <input type='checkbox' id='checkbox".$fd_id.$patient_id."' onclick='update_patient_history_changes(".$fd_id.",".$patient_id.", ".$visit_id.",2)' ".$checked_two." value='".$checked_value_two."'> 
									</div>
									<div class='col-md-3'>
										  016 <input type='checkbox' id='checkbox".$fd_id.$patient_id."' onclick='update_patient_history_changes(".$fd_id.",".$patient_id.", ".$visit_id.",3)' ".$checked_three." value='".$checked_value_three."'> 
									</div>
									<div class='col-md-3'>
										  016 X 022 <input type='checkbox' id='checkbox".$fd_id.$patient_id."' onclick='update_patient_history_changes(".$fd_id.",".$patient_id.", ".$visit_id.",4)' ".$checked_four." value='".$checked_value_four."'> 
									</div>
									



									
									
								</div>
								<div class='row'>
									
									<div class='col-md-1'>
									SS LOWER
									</div>
									<div class='col-md-2'>
										  012 <input type='checkbox' id='checkbox".$fd_id.$patient_id."' onclick='update_patient_history_changes(".$fd_id.",".$patient_id.", ".$visit_id.",5)' ".$checked_five." value='".$checked_value_five."'> 
									</div>
									<div class='col-md-3'>
										  014 <input type='checkbox' id='checkbox".$fd_id.$patient_id."' onclick='update_patient_history_changes(".$fd_id.",".$patient_id.", ".$visit_id.",6)' ".$checked_six." value='".$checked_value_six."'> 
									</div>
									<div class='col-md-3'>
										  016 <input type='checkbox' id='checkbox".$fd_id.$patient_id."' onclick='update_patient_history_changes(".$fd_id.",".$patient_id.", ".$visit_id.",7)' ".$checked_seven." value='".$checked_value_seven."'> 
									</div>
									<div class='col-md-3'>
										  016 X 022 <input type='checkbox' id='checkbox".$fd_id.$patient_id."' onclick='update_patient_history_changes(".$fd_id.",".$patient_id.", ".$visit_id.",8)' ".$checked_eight." value='".$checked_value_eight."'> 
									</div>
									


									
									
									
								</div>";					
		
		
		$count_disease++;
	}
}
?>
<div class="row" style="margin-top: 5px;">	
	<div class="col-md-12">	
		<div class='row'>
			<table class='table table-condensed table-striped table-bordered table-hover'>
				<tr>
					<td colspan='4'><h4 class='left-align'>LEVELLING AND ALIGNMENT</h4></td>
				</tr>
			</table>
		</div>
		<div class="padd">
			<?php echo $health14?>
		</div>
	</div>
	
</div>
<br>
<?php
$patient_health_form13 = $this->dental_model->get_patient_history_forms(14,0,7);

$count_disease = 0;
$health15= '';
if($patient_health_form13->num_rows() > 0)
{
	$patient_health_rs = $patient_health_form13->result();
	$checked_one = '';
	$checked_two = '';
	$checked_three = '';
	$checked_four = '';
	$checked_five = '';

	$checked_value_one = 1;
	$checked_value_two = 2;
	$checked_value_three = 3;
	$checked_value_four = 4;
	$checked_value_five = 5;
	foreach($patient_health_rs as $dis)
	{	
		$fd_id = $dis->patient_history_id;
		$disease = $dis->patient_history_name;
		
		

		$query_result = $this->dental_model->get_patient_history_results($fd_id,$patient_id,$visit_id);

		if($query_result->num_rows() > 0)
		{
			$patient_history_result = $query_result->result();
			foreach ($patient_history_result as $key) {
				# code...
				$patient_history_status = $key->patient_history_status;
				$item_id = $key->item_id;

				if($patient_history_status == 1 AND $item_id == 1)
				{
					$checked_one = "checked='checked'";
					$checked_value_one = 1;
				}
				else if($patient_history_status == 1 AND $item_id == 2)
				{
					$checked_two = "checked='checked'";
					$checked_value_two = 2;
				}
				else if($patient_history_status == 1 AND $item_id == 3)
				{
					$checked_three = "checked='checked'";
					$checked_value_three = 3;
				}
				

			}
		}
		
		$health15 =  $health15."
								<div class='row'>
									
									<div class='col-md-4'>
										  FRICTION <input type='checkbox' id='checkbox".$fd_id.$patient_id."' onclick='update_patient_history_changes(".$fd_id.",".$patient_id.", ".$visit_id.",1)' ".$checked_one." value='".$checked_value_one."'> 
									</div>
									<div class='col-md-4'> 
										  FRICTIONLESS <input type='checkbox' id='checkbox".$fd_id.$patient_id."' onclick='update_patient_history_changes(".$fd_id.",".$patient_id.", ".$visit_id.",2)' ".$checked_two." value='".$checked_value_two."'> 
										
									</div>
									



									
									
								</div>
								";					
		
		
		$count_disease++;
	}
}
?>
<div class="row" style="margin-top: 5px;">	
	<div class="col-md-6">	
		<div class='row'>
			<table class='table table-condensed table-striped table-bordered table-hover'>
				<tr>
					<td colspan='4'><h4 class='left-align'>SPACE CLOSURE MOLAR CORRECTION</h4></td>
				</tr>
			</table>
		</div>
		<div class='padd'>
			<?php echo $health15?>
		</div>
	</div>
	
</div>
<br>
<?php
$patient_health_form14 = $this->dental_model->get_patient_history_forms(14,0,7);

$count_disease = 0;
$health16= '';
if($patient_health_form14->num_rows() > 0)
{
	$patient_health_rs = $patient_health_form14->result();
	$checked_one = '';
	$checked_two = '';
	$checked_three = '';
	$checked_four = '';
	$checked_five = '';
	$checked_six = '';
	$checked_seven = '';
	$checked_eight = '';
	$checked_nine = '';
	$checked_ten = '';
	$checked_eleven = '';
	$checked_twelve = '';

	$checked_value_one = 1;
	$checked_value_two = 2;
	$checked_value_three = 3;
	$checked_value_four = 4;
	$checked_value_five = 5;
	$checked_value_six = 6;
	$checked_value_seven = 7;
	$checked_value_eight = 8;
	$checked_value_nine = 9;
	$checked_value_ten = 10;
	$checked_value_eleven = 11;
	$checked_value_twelve = 12;
	foreach($patient_health_rs as $dis)
	{	
		$fd_id = $dis->patient_history_id;
		$disease = $dis->patient_history_name;
		
		

		$query_result = $this->dental_model->get_patient_history_results($fd_id,$patient_id,$visit_id);

		if($query_result->num_rows() > 0)
		{
			$patient_history_result = $query_result->result();
			foreach ($patient_history_result as $key) {
				# code...
				$patient_history_status = $key->patient_history_status;
				$item_id = $key->item_id;

				if($patient_history_status == 1 AND $item_id == 1)
				{
					$checked_one = "checked='checked'";
					$checked_value_one = 1;
				}
				else if($patient_history_status == 1 AND $item_id == 2)
				{
					$checked_two = "checked='checked'";
					$checked_value_two = 2;
				}
				else if($patient_history_status == 1 AND $item_id == 3)
				{
					$checked_three = "checked='checked'";
					$checked_value_three = 3;
				}
				else if($patient_history_status == 1 AND $item_id == 4)
				{
					$checked_four = "checked='checked'";
					$checked_value_four = 4;
				}
				else if($patient_history_status == 1 AND $item_id == 5)
				{
					$checked_five = "checked='checked'";
					$checked_value_five = 5;
				}
				else if($patient_history_status == 1 AND $item_id == 6)
				{
					$checked_six = "checked='checked'";
					$checked_value_six = 6;
				}
				else if($patient_history_status == 1 AND $item_id == 7)
				{
					$checked_seven = "checked='checked'";
					$checked_value_seven = 7;
				}
				else if($patient_history_status == 1 AND $item_id == 8)
				{
					$checked_eight = "checked='checked'";
					$checked_value_eight = 8;
				}
				else if($patient_history_status == 1 AND $item_id == 9)
				{
					$checked_nine = "checked='checked'";
					$checked_value_nine = 9;
				}
				else if($patient_history_status == 1 AND $item_id == 10)
				{
					$checked_ten = "checked='checked'";
					$checked_value_ten = 10;
				}
				else if($patient_history_status == 1 AND $item_id == 11)
				{
					$checked_eleven = "checked='checked'";
					$checked_value_eleven = 11;
				}
				else if($patient_history_status == 1 AND $item_id == 12)
				{
					$checked_twelve = "checked='checked'";
					$checked_value_twelve = 12;
				}
				

			}
		}
		
		$health16 =  $health16."
								<div class='col-md-6'>
									
									<div class='col-md-12'>
										  FIXED <input type='checkbox' id='checkbox".$fd_id.$patient_id."' onclick='update_patient_history_changes(".$fd_id.",".$patient_id.", ".$visit_id.",1)' ".$checked_one." value='".$checked_value_one."'> 
									</div>
									<div class='col-md-12'> 
										  FSW <input type='checkbox' id='checkbox".$fd_id.$patient_id."' onclick='update_patient_history_changes(".$fd_id.",".$patient_id.", ".$visit_id.",2)' ".$checked_two." value='".$checked_value_two."'> 
										
									</div>
									
								</div>
								<div class='col-md-6'>
									<div class='col-md-12'>
										  REMOVABLE <input type='checkbox' id='checkbox".$fd_id.$patient_id."' onclick='update_patient_history_changes(".$fd_id.",".$patient_id.", ".$visit_id.",3)' ".$checked_three." value='".$checked_value_three."'> 
									</div>
									<div class='col-md-12'> 
										  HAWLEYS APPLIANCE <input type='checkbox' id='checkbox".$fd_id.$patient_id."' onclick='update_patient_history_changes(".$fd_id.",".$patient_id.", ".$visit_id.",4)' ".$checked_four." value='".$checked_value_four."'> 
										
									</div>
									<div class='col-md-12'> 
										  BEGG'S WRAP AROUND <input type='checkbox' id='checkbox".$fd_id.$patient_id."' onclick='update_patient_history_changes(".$fd_id.",".$patient_id.", ".$visit_id.",5)' ".$checked_five." value='".$checked_value_five."'> 
										
									</div>
									<div class='col-md-12'> 
										  ESSIX'S <input type='checkbox' id='checkbox".$fd_id.$patient_id."' onclick='update_patient_history_changes(".$fd_id.",".$patient_id.", ".$visit_id.",6)' ".$checked_six." value='".$checked_value_six."'> 
										
									</div>
								</div>
								";					
		
		
		$count_disease++;
	}
}
?>

<div class="row" style="margin-top: 5px;">	
	<div class="col-md-12">	
		<div class='row'>
			<table class='table table-condensed table-striped table-bordered table-hover'>
				<tr>
					<td colspan='4'><h4 class='left-align'>RETENTION</h4></td>
				</tr>
			</table>
		</div>
		<div class="padd">	
			<?php echo $health16;?>
		</div>

	</div>
	
</div>

