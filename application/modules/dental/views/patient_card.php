<?php
// get visit details


$visit_rs = $this->reception_model->get_visit_details($visit_id);

foreach ($visit_rs as $key) {
	# code...

	$time_end=$key->time_end;
	$time_start=$key->time_start;
	$visit_date=$key->visit_date;
	$personnel_id=$key->personnel_id;
	$room_id=$key->room_id;
	$patient_id=$key->patient_id;
	// $scheme_name=$key->scheme_name;
	$insurance_description=$key->insurance_description;
	$patient_insurance_number=$key->patient_insurance_number;
	$insurance_limit=$key->insurance_limit;
	$principal_member=$key->principal_member;
}
// var_dump($patient_id);die();

$v_data['visit_id'] = $visit_id;
$v_data['patient_id'] = $patient_id;
$v_data['chart_type'] = $chart_type;
?>
 <section class="panel">
	<header class="panel-heading">
		<h2 class="panel-title"><?php echo $patient?></h2>
		<div class="pull-right">
		<?php
			if($mike == 'a')
			{
				?>
				 <a href="<?php echo site_url().'hospital-reports/visit-report'?>" style="margin-top:-50px" class="btn btn-large btn-info fa fa-arrow-left" > Back to Reports </a>
				 <?php

			}else
			{
				?>
				 <a href="<?php echo site_url().'queue'?>" style="margin-top:-50px" class="btn btn-large btn-info fa fa-arrow-left" > Back to Queue </a>
				 <?php
				}
				?>
		</div>
	</header>
	<div class="panel-body">
    
		<div class="center-align">
			<?php
				$error = $this->session->userdata('error_message');
				$validation_error = validation_errors();
				$success = $this->session->userdata('success_message');
				
				if(!empty($error))
				{
					echo '<div class="alert alert-danger">'.$error.'</div>';
					$this->session->unset_userdata('error_message');
				}
				
				if(!empty($validation_error))
				{
					echo '<div class="alert alert-danger">'.$validation_error.'</div>';
				}
				
				if(!empty($success))
				{
					echo '<div class="alert alert-success">'.$success.'</div>';
					$this->session->unset_userdata('success_message');
				}
			?>
		</div>
		
		<?php echo $this->load->view("nurse/allergies_brief", '', TRUE);?>
			
		<div class="clearfix"></div>
		<input type="hidden" name="patient_id" id="patient_id" value="<?php echo $patient_id;?>">
		<input type="hidden" name="visit_id" id="visit_id" value="<?php echo $visit_id;?>">
		 <input type="hidden" name="current_date" id="current_date" value="<?php echo $visit_date;?>">
          <input type="hidden" name="visit_id" id="visit_id" value="<?php echo $visit_id;?>">
		<div class="tabbable" style="margin-bottom: 18px;">
			<ul class="nav nav-tabs nav-justified">
				<li class="active" ><a href="#patient-history" data-toggle="tab">Patient card history</a></li>
				<?php
				if($mike == 'a')
				{
					?>

						<li><a href="#billing-tab" data-toggle="tab">Visit Billing</a></li>
					<?php
				}else
				{
					?>
						<li><a href="#dentine" data-toggle="tab">Patient's Dental</a></li>
						 <li><a href="#chart" data-toggle="tab">Orthodontic Chart</a></li>
				        <li><a href="#prescription" data-toggle="tab">Prescription</a></li>
				        <li><a href="#request" data-toggle="tab">Lab Request</a></li>
						<li><a href="#uploads" data-toggle="tab">Uploads</a></li>						
						<li><a href="#billing-tab" data-toggle="tab">Visit Billing</a></li>
						<li><a href="#visit_trail" data-toggle="tab">Visit Trail</a></li>
					<?php	
				}
				?>

			
			</ul>
			<div class="tab-content" style="padding-bottom: 9px; border-bottom: 1px solid #ddd;">
				
				<div class="tab-pane active" id="patient-history">
					<?php echo $this->load->view("patient_history", $v_data, TRUE);?>
					
					<?php //echo $this->load->view("examination_con", $v_data, TRUE);?>
					<?php //echo $this->load->view("medical_history", '', TRUE);?>


				</div>
				<div class="tab-pane " id="dentine">
					<!-- <div id="page_item"></div> -->
					<?php echo $this->load->view("dental_chart", $v_data, TRUE);?>
				</div>
				<div class="tab-pane " id="chart">
					<!-- <div id="page_item"></div> -->
					<?php echo $this->load->view("examination", $v_data, TRUE);?>
				</div>
				<div class="tab-pane " id="history-patients">
					<?php echo $this->load->view("nurse/patients/lifestyle", '', TRUE);?>
				</div>

					<div class="tab-pane " id="prescription">

					<?php echo $this->load->view("prescription_view", '', TRUE); ?>
					<div id="visit-prescription"></div>
				</div>
					<div class="tab-pane " id="request">

					<?php echo $this->load->view("request", '', TRUE); ?>
					<div id="visit-request"></div>
				</div>
				<div class="tab-pane " id="uploads">
					<?php echo $this->load->view("uploads", '', TRUE);?>
				</div>
				<div class="tab-pane " id="diary">
					<?php echo $this->load->view("patient_appointments", '', TRUE);?>
				</div>
				<div class="tab-pane" id="billing-tab">
					<?php echo $this->load->view("billing", '', TRUE);?>
				</div>
				<div class="tab-pane" id="visit_trail">
					<?php echo $this->load->view("visit_trail", '', TRUE);?>
				</div>

			</div>
		</div>
				  
		<?php
		if($mike == 'a')
		{

		}else
		{
			?>

			<div class="row">
				<div class="center-align"> 
				 
					<div class="col-md-12">
					  <div class="center-align">
						<?php echo form_open("dental/send_to_accounts/".$visit_id, array("class" => "form-horizontal"));?>
						  <input type="submit" class="btn btn-large btn-danger center-align" value="Send To Accounts"/>
						<?php echo form_close();?>
					  </div>
					</div>
				</div>

			</div>
		<?php

		}
		?>
			
	</div>
        
  </section>
  
 
  <script type="text/javascript">
  	
	var config_url = $("#config_url").val();
		
	$(document).ready(function(){
		

	 //  	$.get( config_url+"nurse/get_family_history/<?php echo $visit_id;?>", function( data ) {
		// 	$("#new-nav").html(data);
		// 	$("#checkup_history").html(data);
		// });

		// get_medication(<?php echo $visit_id;?>);
		// prescription_view();

		// get_surgeries(<?php echo $visit_id;?>);

		get_page_item(1,<?php echo $patient_id;?>);
		get_oral_teeth(1,<?php echo $patient_id;?>);
		get_rotation_teeth(1,<?php echo $patient_id;?>);
		
		visit_display_billing(<?php echo $visit_id;?>);

		procedure_lists(<?php echo $visit_id;?>,<?php echo $patient_id;?>);
		window.localStorage.setItem('tooth_item_checked',0);

		get_days_bill(<?php echo $visit_id;?>,<?php echo $patient_id;?>);

		display_notes(<?php echo $visit_id;?>,<?php echo $patient_id;?>,1);
		display_notes(<?php echo $visit_id;?>,<?php echo $patient_id;?>,2);
		display_notes(<?php echo $visit_id;?>,<?php echo $patient_id;?>,3);
		display_notes(<?php echo $visit_id;?>,<?php echo $patient_id;?>,4);
		display_notes(<?php echo $visit_id;?>,<?php echo $patient_id;?>,5);
		display_notes(<?php echo $visit_id;?>,<?php echo $patient_id;?>,6);
		display_notes(<?php echo $visit_id;?>,<?php echo $patient_id;?>,7);
		display_notes(<?php echo $visit_id;?>,<?php echo $patient_id;?>,8);
		display_notes(<?php echo $visit_id;?>,<?php echo $patient_id;?>,9);
		display_notes(<?php echo $visit_id;?>,<?php echo $patient_id;?>,10);
		display_notes(<?php echo $visit_id;?>,<?php echo $patient_id;?>,11);
		display_notes(<?php echo $visit_id;?>,<?php echo $patient_id;?>,12);

		



	});

	function get_medication(visit_id){
    
	    var XMLHttpRequestObject = false;
	        
	    if (window.XMLHttpRequest) {
	    
	        XMLHttpRequestObject = new XMLHttpRequest();
	    } 
	        
	    else if (window.ActiveXObject) {
	        XMLHttpRequestObject = new ActiveXObject("Microsoft.XMLHTTP");
	    }

	    var config_url = document.getElementById("config_url").value;
	    var url = config_url+"nurse/load_medication/"+visit_id;

	    if(XMLHttpRequestObject) {
	        
	        var obj = document.getElementById("medication");
	                
	        XMLHttpRequestObject.open("GET", url);
	                
	        XMLHttpRequestObject.onreadystatechange = function(){
	            
	            if (XMLHttpRequestObject.readyState == 4 && XMLHttpRequestObject.status == 200) {

	                obj.innerHTML = XMLHttpRequestObject.responseText;
	            }
	        }
	                
	        XMLHttpRequestObject.send(null);
	    }
	}
	function get_surgeries(visit_id){
    
	    var XMLHttpRequestObject = false;
	        
	    if (window.XMLHttpRequest) {
	    
	        XMLHttpRequestObject = new XMLHttpRequest();
	    } 
	        
	    else if (window.ActiveXObject) {
	        XMLHttpRequestObject = new ActiveXObject("Microsoft.XMLHTTP");
	    }
	    var config_url = document.getElementById("config_url").value;
	    var url = config_url+"nurse/load_surgeries/"+visit_id;
	    
	    if(XMLHttpRequestObject) {
	        
	        var obj = document.getElementById("surgeries");
	                
	        XMLHttpRequestObject.open("GET", url);
	                
	        XMLHttpRequestObject.onreadystatechange = function(){
	            
	            if (XMLHttpRequestObject.readyState == 4 && XMLHttpRequestObject.status == 200) {

	                obj.innerHTML = XMLHttpRequestObject.responseText;
	            }
	        }
	                
	        XMLHttpRequestObject.send(null);
	    }
	}
	function save_surgery(visit_id){
	    var XMLHttpRequestObject = false;
	        
	    if (window.XMLHttpRequest) {
	    
	        XMLHttpRequestObject = new XMLHttpRequest();
	    } 
	        
	    else if (window.ActiveXObject) {
	        XMLHttpRequestObject = new ActiveXObject("Microsoft.XMLHTTP");
	    }
	    var date = document.getElementById("datepicker").value;
	    var description = document.getElementById("surgery_description").value;
	    var month = document.getElementById("month").value;
	    var config_url = document.getElementById("config_url").value;
	    var url = config_url+"nurse/surgeries/"+date+"/"+description+"/"+month+"/"+visit_id;
	   
	    if(XMLHttpRequestObject) {
	                
	        XMLHttpRequestObject.open("GET", url);
	                
	        XMLHttpRequestObject.onreadystatechange = function(){
	            
	            if (XMLHttpRequestObject.readyState == 4 && XMLHttpRequestObject.status == 200) {

	                get_surgeries(visit_id);
	            }
	        }
	                
	        XMLHttpRequestObject.send(null);
	    }
	}

	function delete_surgery(id, visit_id){
	    //alert(id);
	    var XMLHttpRequestObject = false;
	        
	    if (window.XMLHttpRequest) {
	    
	        XMLHttpRequestObject = new XMLHttpRequest();
	    } 
	        
	    else if (window.ActiveXObject) {
	        XMLHttpRequestObject = new ActiveXObject("Microsoft.XMLHTTP");
	    }
	      var config_url = document.getElementById("config_url").value;
	    var url = config_url+"nurse/delete_surgeries/"+id;
	    
	    if(XMLHttpRequestObject) {
	                
	        XMLHttpRequestObject.open("GET", url);
	                
	        XMLHttpRequestObject.onreadystatechange = function(){
	            
	            if (XMLHttpRequestObject.readyState == 4 && XMLHttpRequestObject.status == 200) {

	                get_surgeries(visit_id);
	            }
	        }
	                
	        XMLHttpRequestObject.send(null);
	    }
	}
	function save_medication(visit_id){
	    var config_url = document.getElementById("config_url").value;
	    var data_url = config_url+"nurse/medication/"+visit_id;
	   
	     var patient_medication = $('#medication_description').val();
	     var patient_medicine_allergies = $('#medicine_allergies').val();
	     var patient_food_allergies = $('#food_allergies').val();
	     var patient_regular_treatment = $('#regular_treatment').val();
	     
	    $.ajax({
	    type:'POST',
	    url: data_url,
	    data:{medication: patient_medication,medicine_allergies: patient_medicine_allergies, food_allergies: patient_food_allergies, regular_treatment: patient_regular_treatment },
	    dataType: 'text',
	    success:function(data){
	     get_medication(visit_id);
	    //obj.innerHTML = XMLHttpRequestObject.responseText;
	    },
	    error: function(xhr, status, error) {
	    //alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);
	    alert(error);
	    }

	    });

	       
	}
  </script>

<script type="text/javascript">
		$(document).ready(function() {
		
		// check_date();
		// load_patient_appointments();

		// tinymce.init({
  //               selector: ".cleditor",
  //               height: "300"
  //           });
  	// alert('dasda');
	});

	function check_date(){
	     var datess=document.getElementById("scheduledate").value;
	     load_schedule();
	     load_patient_appointments_two();
	     if(datess){
		  $('#show_doctor').fadeToggle(1000); return false;
		 }
		 else{
		  alert('Select Date First')
		 }
	}

	function load_schedule(){
		var config_url = $('#config_url').val();
		var datess=document.getElementById("scheduledate").value;
		var doctor= <?php echo $this->session->userdata('personnel_id');?>//document.getElementById("doctor").value;

		var url= config_url+"reception/doc_schedule/"+doctor+"/"+datess;
	
		  $('#doctors_schedule').load(url);
		  $('#doctors_schedule').fadeIn(1000); return false;	
	}
	function load_patient_appointments(){
		var patient_id = $('#patient_id').val();
		var current_date = $('#current_date').val();

		var url= config_url+"reception/patient_schedule/"+patient_id+"/"+current_date;
		
		$('#patient_schedule').load(url);
		$('#patient_schedule').fadeIn(1000); return false;	

		$('#patient_schedule2').load(url);
		$('#patient_schedule2').fadeIn(1000); return false;	
	}
	function load_patient_appointments_two(){
		var patient_id = $('#patient_id').val();
		var current_date = $('#current_date').val();

		var url= config_url+"reception/patient_schedule/"+patient_id+"/"+current_date;
		
		$('#patient_schedule2').load(url);
		$('#patient_schedule2').fadeIn(1000); return false;	
	}
	function schedule_appointment(appointment_id)
	{
		if(appointment_id == '1')
		{
			$('#appointment_details').css('display', 'block');
		}
		else
		{
			$('#appointment_details').css('display', 'none');
		}
	}

	var config_url = $("#config_url").val();
		
	// $(document).ready(function(){
		
	//   	$.get( config_url+"nurse/get_family_history/<?php echo $visit_id;?>", function( data ) {
	// 		$("#new-nav").html(data);
	// 		$("#checkup_history").html(data);
	// 	});
	// });

	function pass_tooth()
    {

     var tooth_item_checked = window.localStorage.getItem('tooth_item_checked');


    
   

		var cavity_status = 0;
		var procedure_status = 2;
		var tooth_id ='';
		var visit_id = document.getElementById("visit_id").value;
		var patient_id = document.getElementById("patient_id").value;
		var teeth_section = '';
		var surface_id = '';

		// alert(tooth_item_checked);

      if(tooth_item_checked == 1)
      {
	      	 var tooth_id = document.getElementById("tooth_id").value;
		     
      	    var teeth_section = document.getElementById("teeth_section").value;
      		var surface_id = document.getElementById("surface_id").value;

      		var radios = document.getElementsByName('cavity_status');
		    var cavity_status = null;
		    for (var i = 0, length = radios.length; i < length; i++)
		    {
		     if (radios[i].checked)
		     {
		      // do whatever you want with the checked radio
		       cavity_status = radios[i].value;
		      // only one radio can be logically checked, don't check the rest
		      break;
		     }
		    }   


		    var radios = document.getElementsByName('procedure_status');
		    var procedure_status = null;
		    for (var i = 0, length = radios.length; i < length; i++)
		    {
		     if (radios[i].checked)
		     {
		      // do whatever you want with the checked radio
		       procedure_status = radios[i].value;
		      // only one radio can be logically checked, don't check the rest
		      break;
		     }
		    }

      }
    

    var radios = document.getElementsByName('service_charges');
    // alert(radios);
    var service_charge = null;
    for (var i = 0, length = radios.length; i < length; i++)
    {
	     if (radios[i].checked)
	     {
	      // do whatever you want with the checked radio
	       service_charge = radios[i].value;
	      // only one radio can be logically checked, don't check the rest
	      break;
	     }
    }
    
	// alert(service_charge);

	var checked = false;
	var checked_response = '';
    if(service_charge != null && procedure_status < 3)
    {
    	checked = true;
    }
	else if(service_charge == null && procedure_status < 3)
	{
		// checked = false;
		checked = true;
		checked_response ='Please select a a service charge on the list';
	}
	else if(service_charge == null && procedure_status == 3)
	{
		checked = true;
		// checked_response ='Please select a a service charge on the list';
	}
	

	if(checked)
	{
    	if(cavity_status == 3 && surface_id === '')
    	{
    		alert('Kindly select a surface of the tooth before you proceed ');
    	}
    	else
    	{


		     var url = "<?php echo base_url();?>dental/save_dentine/"+visit_id+"/"+patient_id;
		     //
		     $.ajax({
		     type:'POST',
		     url: url,
		     data:{tooth_id: tooth_id,patient_id: patient_id,cavity_status: cavity_status,teeth_section: teeth_section,service_charge_id: service_charge,visit_id: visit_id,surface_id:surface_id,plan_status: procedure_status},
		     dataType: 'text',
		     success:function(data){
		       // var prescription_view = document.getElementById("prescription_view");
		       // prescription_view.style.display = 'none';

		         var data = jQuery.parseJSON(data);
		            
		            var status = data.status;

		            if(status == 'success')
		            {
		              // alert(data.message);
		           
		              // display_patient_history(visit_id,patient_id);
		              // close_side_bar(); 
		              if(tooth_item_checked == 1)
		              {


		                if(tooth_id <= 38)
						{
							var newcol = 'chart_type_two';
							get_page_item(7,patient_id,1);
						}
						else
						{
							var newcol = 'chart_type_one';
							get_page_item(7,patient_id,0);
						}
						// alert(newcol);
						// $('a').on('click', function () {
					    $('#' + newcol).prop('checked',true);
					 
						// });
					 }
					 else
					 {
					 	alert(data.message);
					 	visit_display_billing(visit_id);
					 }


		            }
		            else
		            {
		              alert(data.message);
		            }

		     
		     },
		     error: function(xhr, status, error) {
		     alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);
		     
		     }
		     });

	 	}
	}
	else
	{
		alert(checked_response);
	}
	 if(tooth_item_checked == 1)
	 {
	 	check_department_type(tooth_id,teeth_section=null);
	 }
     


     return false;
    }

     function get_page_item(page_id,patient_id,chart_type = null)
    {
        // alert(page_id);
        // get_page_links(page_id,patient_id);
        // if(page_id > 1)
        // {
            var visit_id = <?php echo $visit_id;?>;//window.localStorage.getItem('visit_id');
             // alert(visit_id);
            if(visit_id > 0)
            {

              var visit_id = visit_id;
              // get_page_header(visit_id); 
              var url = "<?php echo base_url();?>dental/get_page_item/"+page_id+"/"+patient_id+"/"+visit_id+"/"+chart_type;  
              // alert(url);
              $.ajax({
              type:'POST',
              url: url,
              data:{page_id: page_id,patient_id: patient_id},
              dataType: 'text',
              success:function(data){
                  var data = jQuery.parseJSON(data);
                  var page_item = data.page_item;

                  // alert(page_item);
                  $('#page_item').html(data.page_item);
              },
              error: function(xhr, status, error) {
              // alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);
                  // display_patient_bill(visit_id);
              }
              });                
            }
            else
            {
              var visit_id = null;

              if(page_id > 1)
              {
                alert("Please select a date of a visit you wish to see");
              }
              else
              {
                var url = "<?php echo base_url();?>dental/get_page_item/"+page_id+"/"+patient_id+"/"+visit_id;  
            
                $.ajax({
                type:'POST',
                url: url,
                data:{page_id: page_id,patient_id: patient_id},
                dataType: 'text',
                success:function(data){
                    var data = jQuery.parseJSON(data);
                    var page_item = data.page_item;
                    $('#page_item').html(data.page_item);
                },
                error: function(xhr, status, error) {
                alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);
                    // display_patient_bill(visit_id);
                }
                });   
              }
              
            }
       
       	visit_display_billing(visit_id);
       // return false;

    }

    function set_tooth_number(teeth_number,teeth_section=null)
  	{ 	

	    var visit_id = document.getElementById("visit_id").value;
	    var patient_id = document.getElementById("patient_id").value;

		var config_url = document.getElementById("config_url").value;
		// alert(teeth_section);
     	var url = config_url+"dental/set_tooth_surfaces/"+teeth_number+"/"+visit_id+"/"+patient_id+"/"+teeth_section;
		// window.alert(data_url);
		$.ajax({
		type:'POST',
		url: url,
		data:{appointment_id: 1},
		dataType: 'text',
		success:function(data){
			var data = jQuery.parseJSON(data);
			// document.getElementById("current-sidebar-div").style.display = "block"; 
			// $("#current-sidebar-div").html(data);
			// alert(data);
			document.getElementById("surface_id").value = data;
			// $("#dentine-list-div").html(data);
		
		},
		error: function(xhr, status, error) {
		//alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);
		alert(error);
		}

		});

  }

    function check_department_type(teeth_number,teeth_section=null)
  	{
  		
  		window.localStorage.setItem('tooth_item_checked',0);

	    var visit_id = document.getElementById("visit_id").value;
	    var patient_id = document.getElementById("patient_id").value;


	    var old_tooth = window.localStorage.getItem('teeth_number_old');
	    // alert(old_tooth);
		if(old_tooth > 0)
		{
			document.getElementById(''+old_tooth+'').style.color = "#000";
		}
		

		// document.getElementById("sidebar-right").style.display = "block"; 
		// document.getElementById("existing-sidebar-div").style.display = "none"; 

		var config_url = document.getElementById("config_url").value;
     	var url = config_url+"dental/display_dental_formula/"+teeth_number+"/"+visit_id+"/"+patient_id+"/"+teeth_section;
		// window.alert(data_url);


		$.ajax({
		type:'POST',
		url: url,
		data:{appointment_id: 1},
		dataType: 'text',
		success:function(data){
			var data = jQuery.parseJSON(data);
			// document.getElementById("current-sidebar-div").style.display = "block"; 
			// $("#current-sidebar-div").html(data);

			// alert(url);
			window.localStorage.setItem('tooth_item_checked',1);
			$("#dentine-list-div").html(data.dental_formula);
			document.getElementById(''+teeth_number+'').style.color = "#FF0000";
			// alert(teeth_number);
			$("#toothnumber").html('TOOTH # '+teeth_number);

			$("#teeth-marker").html(data.teeth_marker);




			window.localStorage.setItem('teeth_number_old',teeth_number);
		},
		error: function(xhr, status, error) {
		//alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);
		alert(error);
		}

		});

		procedure_lists(visit_id,patient_id);

     // var XMLHttpRequestObject = false;
          
     //  if (window.XMLHttpRequest) {
      
     //      XMLHttpRequestObject = new XMLHttpRequest();
     //  } 
          
     //  else if (window.ActiveXObject) {
     //      XMLHttpRequestObject = new ActiveXObject("Microsoft.XMLHTTP");
     //  }
      
     //  var config_url = document.getElementById("config_url").value;
     //  var url = config_url+"dental/display_dental_formula/"+teeth_number+"/"+visit_id+"/"+patient_id+"/"+teeth_section;
     //  // alert(url);
     //  if(XMLHttpRequestObject) {
                  
     //      XMLHttpRequestObject.open("GET", url);
                  
     //      XMLHttpRequestObject.onreadystatechange = function(){
              
     //          if (XMLHttpRequestObject.readyState == 4 && XMLHttpRequestObject.status == 200) {

     //              document.getElementById("dental-formula").innerHTML=XMLHttpRequestObject.responseText;
     //          }
     //      }
                  
     //      XMLHttpRequestObject.send(null);
     //  }
  }




  function save_other_sick_off(visit_id)
	{
		 // start of saving rx
        var config_url = $('#config_url').val();
        var data_url = config_url+"dental/save_other_patient_sickoff/"+visit_id;
        //window.alert(data_url);
         var doctor_notes_rx = tinymce.get('deductions_and_other'+visit_id).getContent();
         // var doctor_notes_rx = $('#deductions_and_other').val();//document.getElementById("vital"+vital_id).value;
        $.ajax({
        type:'POST',
        url: data_url,
        data:{notes: doctor_notes_rx},
        dataType: 'text',
        success:function(data){
        //obj.innerHTML = XMLHttpRequestObject.responseText;
           window.alert("You have successfully updated the payment information");
        },
        error: function(xhr, status, error) {
        //alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);
        alert(error);
        }

        });
      // end of saving rx
	}

  function prescription_view()
  {

    // var myTarget = document.getElementById("add_item");
    // myTarget.style.display = 'block';

    var visit_id = document.getElementById("visit_id").value;
    var patient_id = document.getElementById("patient_id").value;
    // alert(patient_id);
     var XMLHttpRequestObject = false;
          
      if (window.XMLHttpRequest) {
      
          XMLHttpRequestObject = new XMLHttpRequest();
      } 
          
      else if (window.ActiveXObject) {
          XMLHttpRequestObject = new ActiveXObject("Microsoft.XMLHTTP");
      }
      
      var config_url = document.getElementById("config_url").value;
      var url = config_url+"dental/display_patient_prescription/"+visit_id+"/"+patient_id;
      // alert(url);
      if(XMLHttpRequestObject) {
                  
          XMLHttpRequestObject.open("GET", url);
                  
          XMLHttpRequestObject.onreadystatechange = function(){
              
              if (XMLHttpRequestObject.readyState == 4 && XMLHttpRequestObject.status == 200) {

                  document.getElementById("visit-prescription").innerHTML=XMLHttpRequestObject.responseText;
              }
          }
                  
          XMLHttpRequestObject.send(null);
      }
  }


	function save_prescription(patient_id,visit_id)
	{
		 // start of saving rx
		 // var prescription = tinymce.get('#visit_prescription').getContent();
        var config_url = $('#config_url').val();
        var data_url = config_url+"dental/save_prescription/"+patient_id+"/"+visit_id;
        // window.alert(data_url);
         // var prescription = $('#visit_prescription').val();//document.getElementById("vital"+vital_id).value;
         		// console.debug(tinymce.activeEditor.getContent());

        var prescription = tinymce.get('visit_prescription'+visit_id).getContent();

        
			 // alert(visit_id);

         // alert(prescription);
        $.ajax({
        type:'POST',
        url: data_url,
        data:{prescription: prescription},
        dataType: 'text',
        success:function(data){
        //obj.innerHTML = XMLHttpRequestObject.responseText;
           window.alert("You have successfully updated the prescription");
           close_side_bar();
           prescription_view();
        },
        error: function(xhr, status, error) {
        //alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);
        alert(error);
        }

        });
      // end of saving rx
	}
  
	function close_side_bar()
	{
		// $('html').removeClass('sidebar-right-opened');
		document.getElementById("sidebar-right").style.display = "none"; 
		document.getElementById("current-sidebar-div").style.display = "none"; 
		document.getElementById("existing-sidebar-div").style.display = "none"; 
		tinymce.remove();
	}
	function get_chart_type(chart_type)
	{

		var config_url = $('#config_url').val();
		var patient_id = $('#patient_id').val();
        var data_url = config_url+"dental/set_chart_type/"+patient_id+"/"+chart_type;

        
        $.ajax({
        type:'POST',
        url: data_url,
        data:{chart_type: chart_type},
        dataType: 'text',
		success:function(data)
		{

		},
        error: function(xhr, status, error) {
	        //alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);
	        alert(error);

        }

        });


        window.localStorage.setItem('tooth_item_checked',0);
		if(chart_type == 0)
		{
			$('#children-div').css('display', 'block');
			$('#adult-div').css('display', 'none');
		}
		else
		{
			$('#children-div').css('display', 'none');
			$('#adult-div').css('display', 'block');
		}

	}
	function procedure_lists(visit_id,patient_id)
	{

		 // start of saving rx
		 // var prescription = tinymce.get('#visit_prescription').getContent();
        var config_url = $('#config_url').val();
        var data_url = config_url+"dental/get_visit_procedures/"+visit_id+"/"+patient_id;

         var lab_test = $('#q').val();
        
        $.ajax({
        type:'POST',
        url: data_url,
        data:{visit_id: visit_id,query: lab_test},
        dataType: 'text',
		success:function(data)
		{
			$("#procedure-lists").html(data);		
		},
        error: function(xhr, status, error) {
	        //alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);
	        alert(error);
        }

        });
      // end of saving rx
	}
	function update_service_charge_bill(visit_id,patient_id,service_charge_id,amount,visit_type_id)
	{

		 // start of saving rx
		 // var prescription = tinymce.get('#visit_prescription').getContent();
        var config_url = $('#config_url').val();
        var data_url = config_url+"dental/add_visit_bill/"+visit_id+"/"+patient_id+"/"+service_charge_id+"/"+amount+"/"+visit_type_id;

        
        $.ajax({
        type:'POST',
        url: data_url,
        data:{visit_id: visit_id},
        dataType: 'text',
		success:function(data)
		{
			// $("#procedure-lists").html(data);		
			visit_display_billing(visit_id);
		},
        error: function(xhr, status, error) {
	        //alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);
	        alert(error);
        }

        });
      // end of saving rx
	}

	function visit_display_billing(visit_id)
	{



		var config_url = $('#config_url').val();
        var data_url = config_url+"dental/view_billing/"+visit_id;

        // alert(data_url);
        $.ajax({
        type:'POST',
        url: data_url,
        data:{visit_id: visit_id},
        dataType: 'text',
		success:function(data)
		{
			// alert(data);
			$("#billing").html(data);

		},
        error: function(xhr, status, error) {
	        //alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);
	        alert(error);
        }

        });
        var patient_id = $('#patient_id').val();
        // visit_display_billing(visit_id);
        // get_days_bill(visit_id,patient_id);
        // procedure_lists(visit_id,patient_id);
	    
	}




	// accounts


	function change_payer(visit_charge_id, service_charge_id, v_id)
	{

		var res = confirm('Do you want to change who is being billed ? ');

		if(res)
		{

			var config_url = document.getElementById("config_url").value;
		    var data_url = config_url+"accounts/change_payer/"+visit_charge_id+"/"+service_charge_id+"/"+v_id;
		   
		      // var tooth = document.getElementById('tooth'+procedure_id).value;
		     // alert(data_url);
		    $.ajax({
		    type:'POST',
		    url: data_url,
		    data:{visit_charge_id: visit_charge_id},
		    dataType: 'text',
		    success:function(data){
		     // get_medication(visit_id);
		         visit_display_billing(v_id);
		     alert('You have successfully updated your billing');
		    //obj.innerHTML = XMLHttpRequestObject.responseText;
		    },
		    error: function(xhr, status, error) {
		    //alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);
		        visit_display_billing(v_id);
		    	alert(error);
		    }

		    });

		}

	}
	//Calculate procedure total
	function calculatetotal(amount, id, procedure_id, v_id){
	       
	    var units = document.getElementById('units'+id).value;  
	    var billed_amount = document.getElementById('billed_amount'+id).value;  
	   // alert(billed_amount);
	    grand_total(id, units, billed_amount, v_id);

	}
	function grand_total(procedure_id, units, amount, v_id){



		 var config_url = document.getElementById("config_url").value;
	     var data_url = config_url+"accounts/update_service_total/"+procedure_id+"/"+units+"/"+amount+"/"+v_id;
	   
	      // var tooth = document.getElementById('tooth'+procedure_id).value;
	     // alert(data_url);
	    $.ajax({
	    type:'POST',
	    url: data_url,
	    data:{procedure_id: procedure_id},
	    dataType: 'text',
	    success:function(data){
	     // get_medication(visit_id);
	         visit_display_billing(v_id);
	     alert('You have successfully updated your billing');
	    //obj.innerHTML = XMLHttpRequestObject.responseText;
	    },
	    error: function(xhr, status, error) {
	    //alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);
	        visit_display_billing(v_id);
	    alert(error);
	    }

	    });
	}

	function delete_procedure(id, visit_id,dentine_id = null){

		var res = confirm('Are you sure you want to delete this procedure ? ');

		if(res)
		{
			var XMLHttpRequestObject = false;
	        
		    if (window.XMLHttpRequest) {
		    
		        XMLHttpRequestObject = new XMLHttpRequest();
		    } 
		        
		    else if (window.ActiveXObject) {
		        XMLHttpRequestObject = new ActiveXObject("Microsoft.XMLHTTP");
		    }
		     var config_url = document.getElementById("config_url").value;
		    var url = config_url+"nurse/delete_procedure/"+id+"/"+dentine_id;
		    
		    if(XMLHttpRequestObject) {
		                
		        XMLHttpRequestObject.open("GET", url);
		                
		        XMLHttpRequestObject.onreadystatechange = function(){
		            
		            if (XMLHttpRequestObject.readyState == 4 && XMLHttpRequestObject.status == 200) {
		            	close_side_bar();
		                visit_display_billing(visit_id);
		            }
		        }
		                
		        XMLHttpRequestObject.send(null);
		    }
		}
	}

	function get_procedure_information(visit_charge_id,visit_id)
	{

	    var visit_id = document.getElementById("visit_id").value;
	    var patient_id = document.getElementById("patient_id").value;



		document.getElementById("sidebar-right").style.display = "block"; 
		document.getElementById("existing-sidebar-div").style.display = "none"; 

		var config_url = document.getElementById("config_url").value;
     	var url = config_url+"dental/visit_charge_information/"+visit_charge_id+"/"+visit_id;
		// window.alert(url);
		$.ajax({
		type:'POST',
		url: url,
		data:{patient_id: patient_id},
		dataType: 'text',
		success:function(data){

			document.getElementById("current-sidebar-div").style.display = "block"; 
			$("#current-sidebar-div").html(data);

			tinymce.init({
                selector: ".cleditor",
                height: "300"
            });
		
		},
		error: function(xhr, status, error) {
		//alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);
		alert(error);
		}

		});
	}


	function update_visit_charges(visit_id,visit_charge_id)
	{
		var config_url = $('#config_url').val();		

		var notes = tinymce.get('notes'+visit_charge_id).getContent();	
		var units = $('#units'+visit_charge_id).val();
		var teeth_id = $('#teeth_id'+visit_charge_id).val();
		var dentine_id = $('#dentine_id'+visit_charge_id).val();
		var billed_amount = $('#billed_amount'+visit_charge_id).val();

		var patient_id = $('#patient_id'+visit_charge_id).val();

		var radios = document.getElementsByName('work_status');
	    var work_status = null;
	    for (var i = 0, length = radios.length; i < length; i++)
	    {
	     if (radios[i].checked)
	     {
	      // do whatever you want with the checked radio
	       work_status = radios[i].value;
	      // only one radio can be logically checked, don't check the rest
	      break;
	     }
	    }


		var url = config_url+"accounts/update_charges_account/"+visit_id+"/"+visit_charge_id;	
		 
		$.ajax({
		type:'POST',
		url: url,
		data:{notes: notes,visit_id: visit_id,visit_charge_id: visit_charge_id,units: units,billed_amount: billed_amount,work_status: work_status,dentine_id: dentine_id, teeth_id: teeth_id,patient_id: patient_id},
		dataType: 'text',
		
		success:function(data)
		{
		  	var data = jQuery.parseJSON(data);

		  	if(data.message == "success")
			{	

				close_side_bar();	

			}
			else
			{
				alert(data.result);
			}
			visit_display_billing(visit_id);

			if(teeth_id <= 38)
			{
				var newcol = 'chart_type_two';
				get_page_item(7,patient_id,1);
			}
			else
			{
				var newcol = 'chart_type_one';
				get_page_item(7,patient_id,0);
			}
			// alert(newcol);
			// $('a').on('click', function () {
		    $('#' + newcol).prop('checked',true);
					 

		},
		error: function(xhr, status, error) {

				alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);

		}
		});
		 
		visit_display_billing(visit_id);	
	   
		
	// });
	}


	function add_patient_prescription(visit_id,patient_id)
	{



		document.getElementById("sidebar-right").style.display = "block"; 
		document.getElementById("existing-sidebar-div").style.display = "none"; 

		var config_url = document.getElementById("config_url").value;
     	var url = config_url+"dental/patient_prescription/"+visit_id+"/"+patient_id;
		//window.alert(data_url);
		$.ajax({
		type:'POST',
		url: url,
		data:{appointment_id: 1},
		dataType: 'text',
		success:function(data){

			document.getElementById("current-sidebar-div").style.display = "block"; 
			$("#current-sidebar-div").html(data);

			tinymce.init({
                selector: ".cleditor",
                height: "300"
            });
		
		},
		error: function(xhr, status, error) {
		//alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);
		alert(error);
		}

		});
	}

	function sicksheet_view()
	{
		 // var myTarget = document.getElementById("add_item");
    // myTarget.style.display = 'block';

    var visit_id = document.getElementById("visit_id").value;
    var patient_id = document.getElementById("patient_id").value;
    // alert(patient_id);
     var XMLHttpRequestObject = false;
          
      if (window.XMLHttpRequest) {
      
          XMLHttpRequestObject = new XMLHttpRequest();
      } 
          
      else if (window.ActiveXObject) {
          XMLHttpRequestObject = new ActiveXObject("Microsoft.XMLHTTP");
      }
      
      var config_url = document.getElementById("config_url").value;
      var url = config_url+"dental/display_patient_sick_leave/"+visit_id+"/"+patient_id;
      // alert(url);
      if(XMLHttpRequestObject) {
                  
          XMLHttpRequestObject.open("GET", url);
                  
          XMLHttpRequestObject.onreadystatechange = function(){
              
              if (XMLHttpRequestObject.readyState == 4 && XMLHttpRequestObject.status == 200) {

                  document.getElementById("sick-sheet").innerHTML=XMLHttpRequestObject.responseText;
              }
          }
                  
          XMLHttpRequestObject.send(null);
      }
	}


	function save_leave_note(patient_id,visit_id)
	{
		 // start of saving rx
		 // var prescription = tinymce.get('#visit_prescription').getContent();
        var config_url = $('#config_url').val();
        var data_url = config_url+"dental/save_sick_leave_note/"+patient_id+"/"+visit_id;
        // window.alert(data_url);
         // var prescription = $('#visit_prescription').val();//document.getElementById("vital"+vital_id).value;
         		// console.debug(tinymce.activeEditor.getContent());

        var sick_leave = tinymce.get('sick_leave'+visit_id).getContent();

        
			 // alert(visit_id);

         // alert(sick_leave);
        $.ajax({
        type:'POST',
        url: data_url,
        data:{sick_leave: sick_leave},
        dataType: 'text',
        success:function(data){
        //obj.innerHTML = XMLHttpRequestObject.responseText;
           window.alert("You have successfully updated the prescription");
           close_side_bar();
           sicksheet_view();
        },
        error: function(xhr, status, error) {
        //alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);
        alert(error);
        }

        });
      // end of saving rx
	}
	function add_patient_sick_note(visit_id,patient_id)
	{

		document.getElementById("sidebar-right").style.display = "block"; 
		document.getElementById("existing-sidebar-div").style.display = "none"; 

		var config_url = document.getElementById("config_url").value;
     	var url = config_url+"dental/patient_sick_note/"+visit_id+"/"+patient_id;
		//window.alert(data_url);
		$.ajax({
		type:'POST',
		url: url,
		data:{appointment_id: 1},
		dataType: 'text',
		success:function(data){

			document.getElementById("current-sidebar-div").style.display = "block"; 
			$("#current-sidebar-div").html(data);

			tinymce.init({
                selector: ".cleditor",
                height: "300"
            });
		
		},
		error: function(xhr, status, error) {
		//alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);
		alert(error);
		}

		});
	}


	// uploads


	function uploads_view()
	{
		 // var myTarget = document.getElementById("add_item");
    // myTarget.style.display = 'block';

    var visit_id = document.getElementById("visit_id").value;
    var patient_id = document.getElementById("patient_id").value;
    // alert(patient_id);
     var XMLHttpRequestObject = false;
          
      if (window.XMLHttpRequest) {
      
          XMLHttpRequestObject = new XMLHttpRequest();
      } 
          
      else if (window.ActiveXObject) {
          XMLHttpRequestObject = new ActiveXObject("Microsoft.XMLHTTP");
      }
      
      var config_url = document.getElementById("config_url").value;
      var url = config_url+"dental/display_patient_uploads/"+visit_id+"/"+patient_id;
      // alert(url);
      if(XMLHttpRequestObject) {
                  
          XMLHttpRequestObject.open("GET", url);
                  
          XMLHttpRequestObject.onreadystatechange = function(){
              
              if (XMLHttpRequestObject.readyState == 4 && XMLHttpRequestObject.status == 200) {

                  document.getElementById("patients-uploads").innerHTML=XMLHttpRequestObject.responseText;
              }
          }
                  
          XMLHttpRequestObject.send(null);
      }
	}


	$(document).on("submit","form#uploads-form",function(e)
	{
		e.preventDefault();
		
		var form_data = new FormData(this);

		// alert(form_data);
		var patient_id = $('#patient_id').val();
		var visit_id = $('#visit_id').val();

		var config_url = $('#config_url').val();	

		var url = config_url+"dental/upload_documents/"+patient_id+"/"+visit_id;
		
		 
	   $.ajax({
	   type:'POST',
	   url: url,
	   data:form_data,
	   dataType: 'text',
	   processData: false,
	   contentType: false,
       success:function(data){
        //obj.innerHTML = XMLHttpRequestObject.responseText;
         
	      var data = jQuery.parseJSON(data);
	    
	      	if(data.message == "success")
			{
				
				
				close_side_bar();
				uploads_view();
				
			}
			else
			{
				alert(data.result);
			}
	   
	   },
	   error: function(xhr, status, error) {
	   alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);
	   
	   }
	   });
		 
		
	   
		
	});
	function add_patient_upload(visit_id,patient_id)
	{

		document.getElementById("sidebar-right").style.display = "block"; 
		document.getElementById("existing-sidebar-div").style.display = "none"; 

		var config_url = document.getElementById("config_url").value;
     	var url = config_url+"dental/patient_uploads/"+visit_id+"/"+patient_id;
		//window.alert(data_url);
		$.ajax({
		type:'POST',
		url: url,
		data:{appointment_id: 1},
		dataType: 'text',
		success:function(data){

			document.getElementById("current-sidebar-div").style.display = "block"; 
			$("#current-sidebar-div").html(data);

			tinymce.init({
                selector: ".cleditor",
                height: "300"
            });
		
		},
		error: function(xhr, status, error) {
		//alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);
		alert(error);
		}

		});
	}
	function delete_upload_file(document_upload_id,visit_id)
	{
		var res = confirm('Are you sure you want to delete this file ? ');

		if(res)
		{
			var config_url = document.getElementById("config_url").value;
	     	var url = config_url+"dental/delete_document_scan/"+document_upload_id+"/"+visit_id;
			//window.alert(data_url);
			$.ajax({
			type:'POST',
			url: url,
			data:{appointment_id: 1},
			dataType: 'text',
			success:function(data){

				uploads_view();
			},
			error: function(xhr, status, error) {
			//alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);
			alert(error);
			}

			});

		}
		
		
	}


	 function prescription_view()
  {

    // var myTarget = document.getElementById("add_item");
    // myTarget.style.display = 'block';

    var visit_id = document.getElementById("visit_id").value;
    var patient_id = document.getElementById("patient_id").value;
    // alert(patient_id);
     var XMLHttpRequestObject = false;
          
      if (window.XMLHttpRequest) {
      
          XMLHttpRequestObject = new XMLHttpRequest();
      } 
          
      else if (window.ActiveXObject) {
          XMLHttpRequestObject = new ActiveXObject("Microsoft.XMLHTTP");
      }
      
      var config_url = document.getElementById("config_url").value;
      var url = config_url+"dental/display_patient_prescription/"+visit_id+"/"+patient_id;
      // alert(url);
      if(XMLHttpRequestObject) {
                  
          XMLHttpRequestObject.open("GET", url);
                  
          XMLHttpRequestObject.onreadystatechange = function(){
              
              if (XMLHttpRequestObject.readyState == 4 && XMLHttpRequestObject.status == 200) {

                  document.getElementById("visit-prescription").innerHTML=XMLHttpRequestObject.responseText;
              }
          }
                  
          XMLHttpRequestObject.send(null);
      }
  }

	 function request()
  {

    // var myTarget = document.getElementById("add_item");
    // myTarget.style.display = 'block';

    var visit_id = document.getElementById("visit_id").value;
    var patient_id = document.getElementById("patient_id").value;
    // alert(patient_id);
     var XMLHttpRequestObject = false;
          
      if (window.XMLHttpRequest) {
      
          XMLHttpRequestObject = new XMLHttpRequest();
      } 
          
      else if (window.ActiveXObject) {
          XMLHttpRequestObject = new ActiveXObject("Microsoft.XMLHTTP");
      }
      
      var config_url = document.getElementById("config_url").value;
      var url = config_url+"dental/display_patient_request/"+visit_id+"/"+patient_id;
      // alert(url);
      if(XMLHttpRequestObject) {
                  
          XMLHttpRequestObject.open("GET", url);
                  
          XMLHttpRequestObject.onreadystatechange = function(){
              
              if (XMLHttpRequestObject.readyState == 4 && XMLHttpRequestObject.status == 200) {

                  document.getElementById("visit-request").innerHTML=XMLHttpRequestObject.responseText;
              }
          }
                  
          XMLHttpRequestObject.send(null);
      }
  }


	 function xray()
  {

    // var myTarget = document.getElementById("add_item");
    // myTarget.style.display = 'block';

    var visit_id = document.getElementById("visit_id").value;
    var patient_id = document.getElementById("patient_id").value;
    // alert(patient_id);
     var XMLHttpRequestObject = false;
          
      if (window.XMLHttpRequest) {
      
          XMLHttpRequestObject = new XMLHttpRequest();
      } 
          
      else if (window.ActiveXObject) {
          XMLHttpRequestObject = new ActiveXObject("Microsoft.XMLHTTP");
      }
      
      var config_url = document.getElementById("config_url").value;
      var url = config_url+"dental/display_patient_xray/"+visit_id+"/"+patient_id;
      // alert(url);
      if(XMLHttpRequestObject) {
                  
          XMLHttpRequestObject.open("GET", url);
                  
          XMLHttpRequestObject.onreadystatechange = function(){
              
              if (XMLHttpRequestObject.readyState == 4 && XMLHttpRequestObject.status == 200) {

                  document.getElementById("visit-xray").innerHTML=XMLHttpRequestObject.responseText;
              }
          }
                  
          XMLHttpRequestObject.send(null);
      }
  }
  

  	 function refferal()
  {

    // var myTarget = document.getElementById("add_item");
    // myTarget.style.display = 'block';

    var visit_id = document.getElementById("visit_id").value;
    var patient_id = document.getElementById("patient_id").value;
    // alert(patient_id);
     var XMLHttpRequestObject = false;
          
      if (window.XMLHttpRequest) {
      
          XMLHttpRequestObject = new XMLHttpRequest();
      } 
          
      else if (window.ActiveXObject) {
          XMLHttpRequestObject = new ActiveXObject("Microsoft.XMLHTTP");
      }
      
      var config_url = document.getElementById("config_url").value;
      var url = config_url+"dental/display_patient_xray/"+visit_id+"/"+patient_id;
      // alert(url);
      if(XMLHttpRequestObject) {
                  
          XMLHttpRequestObject.open("GET", url);
                  
          XMLHttpRequestObject.onreadystatechange = function(){
              
              if (XMLHttpRequestObject.readyState == 4 && XMLHttpRequestObject.status == 200) {

                  document.getElementById("visit-refferal").innerHTML=XMLHttpRequestObject.responseText;
              }
          }
                  
          XMLHttpRequestObject.send(null);
      }
  }


	function save_prescription(patient_id,visit_id)
	{
		 // start of saving rx
		 // var prescription = tinymce.get('#visit_prescription').getContent();
        var config_url = $('#config_url').val();
        var data_url = config_url+"dental/save_prescription/"+patient_id+"/"+visit_id;
        // window.alert(data_url);
         // var prescription = $('#visit_prescription').val();//document.getElementById("vital"+vital_id).value;
         		// console.debug(tinymce.activeEditor.getContent());

        var prescription = tinymce.get('visit_prescription'+visit_id).getContent();

        
			 // alert(visit_id);

         // alert(prescription);
        $.ajax({
        type:'POST',
        url: data_url,
        data:{prescription: prescription},
        dataType: 'text',
        success:function(data){
        //obj.innerHTML = XMLHttpRequestObject.responseText;
           window.alert("You have successfully updated the prescription");
           prescription_view();
        },
        error: function(xhr, status, error) {
        //alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);
        alert(error);
        }

        });
      // end of saving rx
	}


	function save_request(patient_id,visit_id)
	{
		 // start of saving rx
		 // var prescription = tinymce.get('#visit_prescription').getContent();
        var config_url = $('#config_url').val();
        var data_url = config_url+"dental/save_request/"+patient_id+"/"+visit_id;
        // window.alert(data_url);
         // var prescription = $('#visit_prescription').val();//document.getElementById("vital"+vital_id).value;
         		// console.debug(tinymce.activeEditor.getContent());

        var request = tinymce.get('visit_request'+visit_id).getContent();

        
			 // alert(visit_id);

         // alert(prescription);
        $.ajax({
        type:'POST',
        url: data_url,
        data:{request: request},
        dataType: 'text',
        success:function(data){
        //obj.innerHTML = XMLHttpRequestObject.responseText;
           window.alert("You have successfully updated the lab request");
           request();
        },
        error: function(xhr, status, error) {
        //alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);
        alert(error);
        }

        });
      // end of saving rx
	}

	function bill_charge(visit_charge_id,visit_id,patient_id)
	{

		var res = confirm('Are you sure you want to bill this procedure ? ');

		if(res)
		{
			var config_url = document.getElementById("config_url").value;
	     	var url = config_url+"dental/bill_procedure/"+visit_charge_id;
			//window.alert(data_url);
			$.ajax({
			type:'POST',
			url: url,
			data:{appointment_id: 1},
			dataType: 'text',
			success:function(data){

				visit_display_billing(visit_id);


			},
			error: function(xhr, status, error) {
			//alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);
			alert(error);
			}

			});

		}
		get_days_bill(visit_id,patient_id);
	}

	function unbill_charge(visit_charge_id,visit_id,patient_id)
	{

		var res = confirm('Are you sure you want to unbill this procedure ? ');

		if(res)
		{
			var config_url = document.getElementById("config_url").value;
	     	var url = config_url+"dental/remove_bill_procedure/"+visit_charge_id;
			//window.alert(data_url);
			$.ajax({
			type:'POST',
			url: url,
			data:{appointment_id: 1},
			dataType: 'text',
			success:function(data){

				visit_display_billing(visit_id);

			},
			error: function(xhr, status, error) {
			//alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);
			alert(error);
			}

			});

		}
		get_days_bill(visit_id,patient_id);
	}
	function get_days_bill(visit_id,patient_id)
	{
		var config_url = $('#config_url').val();
        var data_url = config_url+"dental/view_days_billing/"+patient_id+"/"+visit_id;

        // alert(data_url);
        $.ajax({
        type:'POST',
        url: data_url,
        data:{visit_id: visit_id},
        dataType: 'text',
		success:function(data)
		{
			// alert(data);
			$("#days-bill").html(data);		
		},
        error: function(xhr, status, error) {
	        //alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);
	        alert(error);
        }

        });
	}

	function add_new_procedure(visit_id, patient_id)
	{

		$('#procedure-div').css('display', 'none');
		// $('#procedure-list-div').css('display', 'none');	
		$('#service_charge-div').css('display', 'block');	
		$('#procedure-search-div').css('display', 'none');	
		$('#new-button-div').css('display', 'none');	
		$('#add-button-div').css('display', 'block');
		
		
	}

	function back_procedure_list(visit_id, patient_id)
	{

		$('#procedure-div').css('display', 'block');
		// $('#procedure-list-div').css('display', 'block');

		$('#service_charge-div').css('display', 'none');	
		$('#procedure-search-div').css('display', 'block');	
		$('#new-button-div').css('display', 'block');	
		$('#add-button-div').css('display', 'none');	
		
	}
	function add_service_charge(visit_id, patient_id)
	{

		var res = confirm('Are you sure you want to add this as a service charge ?');


		if(res)
		{
			var config_url = $('#config_url').val();

			var service_charge_name = $('#service_charge_name').val();
	        var data_url = config_url+"dental/add_new_service";

	        // alert(data_url);
	        $.ajax({
	        type:'POST',
	        url: data_url,
	        data:{service_charge_name: service_charge_name},
	        dataType: 'text',
			success:function(data)
			{
				var data = jQuery.parseJSON(data);
	    
				// alert(data);

				if(data.status == "success")
				{
					alert('You have successfully added the procedure');

					back_procedure_list(visit_id, patient_id);

				}
				else
				{
					alert(data.message);
				}
				
					
			},
	        error: function(xhr, status, error) {
		        //alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);
		        alert(error);
	        }

	        });
		}
	}

	$(document).on("submit", "form#medical_form", function (e) 
    {
      var visit_id = $('#visit_id').val(); 
      var patient_id = $('#patient_id').val(); 


      var config_url = document.getElementById("config_url").value;
      var data_url_investigations = config_url+"dental/submit_medical_history/"+visit_id+"/"+patient_id;

      var physicians_care= $('input[id="physicians_care'+visit_id+'"]:checked').val(); 
      var physician_name= $('#physician_name'+visit_id).val(); 
      var physician_phone= $('#physician_phone'+visit_id).val(); 
      var current_mediction= $('#current_mediction'+visit_id).val(); 
      var cheif= $('#cheif'+visit_id).val(); 
      var face= $('#face'+visit_id).val(); 
      var bites= $('#bites'+visit_id).val(); 
      var anterior= $('#anterior'+visit_id).val(); 
      var lateral= $('#lateral'+visit_id).val();
      var habits= $('#habits'+visit_id).val();
      var cross= $('#cross'+visit_id).val(); 
      var jet= $('#jet'+visit_id).val();   
      var over= $('#over'+visit_id).val();   
      var photos= $('#photos'+visit_id).val();
      var check= $('#check'+visit_id).val();
      var mid= $('#mid'+visit_id).val(); 
      var upper= $('#upper'+visit_id).val();
      var lower= $('#lower'+visit_id).val(); 
      var sulcus= $('#sulcus'+visit_id).val(); 
      var plan= $('#plan'+visit_id).val();
      var method= $('#method'+visit_id).val();
      var amount= $('#amount'+visit_id).val(); 
      var pay= $('#pay'+visit_id).val();                                    
      var medical_conditions= $('#medical_conditions'+visit_id).val(); 
      var allergic_reaction_causes= $('#allergic_reaction_causes'+visit_id).val(); 
      var birth_pills=$('input[id="birth_pills'+visit_id+'"]:checked').val();
      var pregnant=$('input[id="pregnant'+visit_id+'"]:checked').val();
      var nursing=$('input[id="nursing'+visit_id+'"]:checked').val();
      var asprin=$('input[id="asprin'+visit_id+'"]:checked').val();
      var erythromycin=$('input[id="erythromycin'+visit_id+'"]:checked').val();
      var sedatives=$('input[id="sedatives'+visit_id+'"]:checked').val();
      var barbiturates=$('input[id="barbiturates'+visit_id+'"]:checked').val();
      var metals=$('input[id="metals'+visit_id+'"]:checked').val();
      var sulpha_drugs=$('input[id="sulpha_drugs'+visit_id+'"]:checked').val();
      var codeine=$('input[id="codeine'+visit_id+'"]:checked').val();
      var latex=$('input[id="latex'+visit_id+'"]:checked').val();
      var tetracycline=$('input[id="tetracycline'+visit_id+'"]:checked').val();
      var dental_anesthetics=$('input[id="dental_anesthetics'+visit_id+'"]:checked').val();
      var penicillin=$('input[id="penicillin'+visit_id+'"]:checked').val();
      var liver_disease=$('input[id="liver_disease'+visit_id+'"]:checked').val();
      var diabetes=$('input[id="diabetes'+visit_id+'"]:checked').val();
      var hemophilia=$('input[id="hemophilia'+visit_id+'"]:checked').val();
      var asthma=$('input[id="asthma'+visit_id+'"]:checked').val();
      var glaucoma=$('input[id="glaucoma'+visit_id+'"]:checked').val();
      var migraines=$('input[id="migraines'+visit_id+'"]:checked').val();
      var chemotherapy=$('input[id="chemotherapy'+visit_id+'"]:checked').val();
      var colitis=$('input[id="colitis'+visit_id+'"]:checked').val();
      var lupus=$('input[id="lupus'+visit_id+'"]:checked').val();
      var hepatitis=$('input[id="hepatitis'+visit_id+'"]:checked').val();
      var seizures=$('input[id="seizures'+visit_id+'"]:checked').val();
      var hiv=$('input[id="hiv'+visit_id+'"]:checked').val();
      var chest_conditions=$('input[id="chest_conditions'+visit_id+'"]:checked').val();
      var heart_conditions=$('input[id="heart_conditions'+visit_id+'"]:checked').val();
      var kidney_problems=$('input[id="kidney_problems'+visit_id+'"]:checked').val();
      var anemia=$('input[id="anemia'+visit_id+'"]:checked').val();
      var artificial_bones=$('input[id="artificial_bones'+visit_id+'"]:checked').val();
      var radiation_treatment=$('input[id="radiation_treatment'+visit_id+'"]:checked').val();
      var high_blood_pressure=$('input[id="high_blood_pressure'+visit_id+'"]:checked').val();
      var control_pills=$('input[id="control_pills'+visit_id+'"]:checked').val();


        $.ajax({
        type:'POST',
        url: data_url_investigations,
        data:{physicians_care: physicians_care,physician_name: physician_name, cheif: cheif,face: face,bites: bites,anterior: anterior,lateral: lateral,habits: habits,bites: bites,cross: cross,jet: jet,over: over,photos: photos,check: check,mid: mid,upper: upper,lower: lower,sulcus: sulcus,plan: plan,method: method,
        	amount: amount, pay: pay,physician_phone: physician_phone, current_mediction: current_mediction,
          allergic_reaction_causes: allergic_reaction_causes,birth_pills: birth_pills,pregnant: pregnant,nursing: nursing,asprin: asprin,erythromycin: erythromycin,sedatives: sedatives,barbiturates: barbiturates,metals: metals,sulpha_drugs: sulpha_drugs,codeine: codeine,latex: latex,tetracycline: tetracycline,dental_anesthetics: dental_anesthetics,penicillin: penicillin,liver_disease: liver_disease,diabetes: diabetes,hemophilia: hemophilia, asthma: asthma, glaucoma: glaucoma, migraines: migraines, chemotherapy: chemotherapy, colitis: colitis, lupus: lupus, hepatitis: hepatitis, seizures: seizures, hiv: hiv, chest_conditions: chest_conditions, heart_conditions: heart_conditions, kidney_problems: kidney_problems, anemia: anemia, artificial_bones: artificial_bones, radiation_treatment: radiation_treatment, high_blood_pressure: high_blood_pressure,medical_conditions: medical_conditions,control_pills: control_pills

          },
        dataType: 'text',
        success:function(data){
        //obj.innerHTML = XMLHttpRequestObject.responseText;
          // var reload = config_url+'patient-card/'+visit_id+'/medical';
          // window.location.href = reload;
          // get_page_item(2,patient_id);
          var data = jQuery.parseJSON(data);

          window.alert(data.message);
        },
        error: function(xhr, status, error) {
        //alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);
        alert(error);
        }

        });
      // end of saving investgations
      return false;
    })

	$(document).on("submit", "form#medical_form", function (e) 
    {
      var visit_id = $('#visit_id').val(); 
      var patient_id = $('#patient_id').val(); 


      var config_url = document.getElementById("config_url").value;
      var data_url_investigations = config_url+"dental/submit_shaw_history/"+visit_id+"/"+patient_id;

      var physicians_care= $('input[id="physicians_care'+visit_id+'"]:checked').val(); 
      var physician_name= $('#physician_name'+visit_id).val(); 
      var physician_phone= $('#physician_phone'+visit_id).val(); 
      var current_mediction= $('#current_mediction'+visit_id).val(); 
      var cheif= $('#cheif'+visit_id).val(); 
      var face= $('#face'+visit_id).val(); 
      var bites= $('#bites'+visit_id).val(); 
      var anterior= $('#anterior'+visit_id).val(); 
      var lateral= $('#lateral'+visit_id).val();
      var habits= $('#habits'+visit_id).val();
      var cross= $('#cross'+visit_id).val(); 
      var jet= $('#jet'+visit_id).val();   
      var over= $('#over'+visit_id).val();   
      var photos= $('#photos'+visit_id).val();
      var check= $('#check'+visit_id).val();
      var mid= $('#mid'+visit_id).val(); 
      var upper= $('#upper'+visit_id).val();
      var lower= $('#lower'+visit_id).val(); 
      var sulcus= $('#sulcus'+visit_id).val(); 
      var plan= $('#plan'+visit_id).val();
      var method= $('#method'+visit_id).val();
      var amount= $('#amount'+visit_id).val(); 
      var pay= $('#pay'+visit_id).val();  
      var ufour= $('#ufour'+visit_id).val();
      var ufourr= $('#ufourr'+visit_id).val();
      var ufourm= $('#ufourm'+visit_id).val();   
      var usix= $('#usix'+visit_id).val();
      var usixx= $('#usixx'+visit_id).val();
      var usixm= $('#usixm'+visit_id).val();
      var lfour= $('#lfour'+visit_id).val();
      var lfourr= $('#lfourr'+visit_id).val();
      var lfourm= $('#lfourm'+visit_id).val();  
      var lsix= $('#lsix'+visit_id).val();
      var lsixx= $('#lsixx'+visit_id).val();
      var lsixm= $('#lsixm'+visit_id).val();
      var sna= $('#sna'+visit_id).val();  
      var snb= $('#snb'+visit_id).val();  
      var air= $('#air'+visit_id).val();
      var ufouw= $('#ufouw'+visit_id).val();                                               
      var medical_conditions= $('#medical_conditions'+visit_id).val(); 
      var allergic_reaction_causes= $('#allergic_reaction_causes'+visit_id).val(); 
      var birth_pills=$('input[id="birth_pills'+visit_id+'"]:checked').val();
      var pregnant=$('input[id="pregnant'+visit_id+'"]:checked').val();
      var nursing=$('input[id="nursing'+visit_id+'"]:checked').val();
      var asprin=$('input[id="asprin'+visit_id+'"]:checked').val();
      var erythromycin=$('input[id="erythromycin'+visit_id+'"]:checked').val();
      var sedatives=$('input[id="sedatives'+visit_id+'"]:checked').val();
      var barbiturates=$('input[id="barbiturates'+visit_id+'"]:checked').val();
      var metals=$('input[id="metals'+visit_id+'"]:checked').val();
      var sulpha_drugs=$('input[id="sulpha_drugs'+visit_id+'"]:checked').val();
      var codeine=$('input[id="codeine'+visit_id+'"]:checked').val();
      var latex=$('input[id="latex'+visit_id+'"]:checked').val();
      var tetracycline=$('input[id="tetracycline'+visit_id+'"]:checked').val();
      var dental_anesthetics=$('input[id="dental_anesthetics'+visit_id+'"]:checked').val();
      var penicillin=$('input[id="penicillin'+visit_id+'"]:checked').val();
      var liver_disease=$('input[id="liver_disease'+visit_id+'"]:checked').val();
      var diabetes=$('input[id="diabetes'+visit_id+'"]:checked').val();
      var hemophilia=$('input[id="hemophilia'+visit_id+'"]:checked').val();
      var asthma=$('input[id="asthma'+visit_id+'"]:checked').val();
      var glaucoma=$('input[id="glaucoma'+visit_id+'"]:checked').val();
      var migraines=$('input[id="migraines'+visit_id+'"]:checked').val();
      var chemotherapy=$('input[id="chemotherapy'+visit_id+'"]:checked').val();
      var colitis=$('input[id="colitis'+visit_id+'"]:checked').val();
      var lupus=$('input[id="lupus'+visit_id+'"]:checked').val();
      var hepatitis=$('input[id="hepatitis'+visit_id+'"]:checked').val();
      var seizures=$('input[id="seizures'+visit_id+'"]:checked').val();
      var hiv=$('input[id="hiv'+visit_id+'"]:checked').val();
      var chest_conditions=$('input[id="chest_conditions'+visit_id+'"]:checked').val();
      var heart_conditions=$('input[id="heart_conditions'+visit_id+'"]:checked').val();
      var kidney_problems=$('input[id="kidney_problems'+visit_id+'"]:checked').val();
      var anemia=$('input[id="anemia'+visit_id+'"]:checked').val();
      var artificial_bones=$('input[id="artificial_bones'+visit_id+'"]:checked').val();
      var radiation_treatment=$('input[id="radiation_treatment'+visit_id+'"]:checked').val();
      var high_blood_pressure=$('input[id="high_blood_pressure'+visit_id+'"]:checked').val();
      var control_pills=$('input[id="control_pills'+visit_id+'"]:checked').val();


        $.ajax({
        type:'POST',
        url: data_url_investigations,
        data:{physicians_care: physicians_care,physician_name: physician_name, cheif: cheif,face: face,bites: bites,anterior: anterior,lateral: lateral,habits: habits,bites: bites,cross: cross,jet: jet,over: over,photos: photos,check: check,mid: mid,upper: upper,lower: lower,sulcus: sulcus,plan: plan,method: method,
        	amount: amount, pay: pay, ufour: ufour,ufourr: ufourr,ufourm: ufourm,usix: usix,usixx: usixx,usixm: usixm,lfour: lfour,lfourr: lfourr,lfourm: lfourm,lsix: lsix,lsixx: lsixx,lsixm: lsixm,ufouw: ufouw,sna: sna,snb: snb,air: air,ufourm: ufourm,physician_phone: physician_phone, current_mediction: current_mediction,
          allergic_reaction_causes: allergic_reaction_causes,birth_pills: birth_pills,pregnant: pregnant,nursing: nursing,asprin: asprin,erythromycin: erythromycin,sedatives: sedatives,barbiturates: barbiturates,metals: metals,sulpha_drugs: sulpha_drugs,codeine: codeine,latex: latex,tetracycline: tetracycline,dental_anesthetics: dental_anesthetics,penicillin: penicillin,liver_disease: liver_disease,diabetes: diabetes,hemophilia: hemophilia, asthma: asthma, glaucoma: glaucoma, migraines: migraines, chemotherapy: chemotherapy, colitis: colitis, lupus: lupus, hepatitis: hepatitis, seizures: seizures, hiv: hiv, chest_conditions: chest_conditions, heart_conditions: heart_conditions, kidney_problems: kidney_problems, anemia: anemia, artificial_bones: artificial_bones, radiation_treatment: radiation_treatment, high_blood_pressure: high_blood_pressure,medical_conditions: medical_conditions,control_pills: control_pills

          },
        dataType: 'text',
        success:function(data){
        //obj.innerHTML = XMLHttpRequestObject.responseText;
          // var reload = config_url+'patient-card/'+visit_id+'/medical';
          // window.location.href = reload;
          // get_page_item(2,patient_id);
          var data = jQuery.parseJSON(data);

          window.alert(data.message);
        },
        error: function(xhr, status, error) {
        //alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);
        alert(error);
        }

        });
      // end of saving investgations
      return false;
    })


	
  	function update_patient_history_changes(patient_history_id,patient_id,visit_id,value_change)
  	{
  		 var checkedValue = $('#checkbox'+patient_history_id+patient_id).val();
  		 // alert(checkedValue);
  		 var XMLHttpRequestObject = false;
		 
		if (window.XMLHttpRequest) {

		 XMLHttpRequestObject = new XMLHttpRequest();
		} 
		 
		else if (window.ActiveXObject) {
		 XMLHttpRequestObject = new ActiveXObject("Microsoft.XMLHTTP");
		} 
		var config_url = $('#config_url').val();
		var url = config_url+"dental/update_patient_history_changes/"+patient_history_id+"/"+patient_id+"/"+visit_id+"/"+checkedValue+"/"+value_change;
		// alert(url);
		if(XMLHttpRequestObject) {
		     
		 XMLHttpRequestObject.open("GET", url);
		     
		 XMLHttpRequestObject.onreadystatechange = function(){
		   
		   if (XMLHttpRequestObject.readyState == 4 && XMLHttpRequestObject.status == 200) {
		   
		   }
		 }
		 
		 XMLHttpRequestObject.send(null);
		}
  	}

   function display_notes(visit_id,patient_id,type)
   {
   
     var XMLHttpRequestObject = false;
         
     if (window.XMLHttpRequest) {
     
         XMLHttpRequestObject = new XMLHttpRequest();
     } 
         
     else if (window.ActiveXObject) {
         XMLHttpRequestObject = new ActiveXObject("Microsoft.XMLHTTP");
     }
     
     var config_url = document.getElementById("config_url").value;
     var url = config_url+"dental/display_notes/"+visit_id+"/"+type+"/"+patient_id;
     
     if(XMLHttpRequestObject) {
                 
         XMLHttpRequestObject.open("GET", url);
                 
         XMLHttpRequestObject.onreadystatechange = function(){
             
             if (XMLHttpRequestObject.readyState == 4 && XMLHttpRequestObject.status == 200) {
                if(type == 1)
                {

                 document.getElementById("facial-asymmetry-description").innerHTML=XMLHttpRequestObject.responseText;
                }
                else if(type == 2)
                {
                  document.getElementById("intra-oral-description").innerHTML=XMLHttpRequestObject.responseText;
                }
                else if(type == 3)
                {
                  document.getElementById("crossibet-description").innerHTML=XMLHttpRequestObject.responseText;
                }
                 else if(type == 4)
                {
                  document.getElementById("opg-description").innerHTML=XMLHttpRequestObject.responseText;
                }
                 else if(type == 5)
                {
                  document.getElementById("ceph-description").innerHTML=XMLHttpRequestObject.responseText;
                }
                 else if(type == 6)
                {
                  document.getElementById("pa-description").innerHTML=XMLHttpRequestObject.responseText;
                }
                 else if(type == 7)
                {
                  document.getElementById("bw-description").innerHTML=XMLHttpRequestObject.responseText;
                }
                 else if(type == 8)
                {
                  document.getElementById("diagnosis-description").innerHTML=XMLHttpRequestObject.responseText;
                }
                 else if(type == 9)
                {
                  document.getElementById("problem-description").innerHTML=XMLHttpRequestObject.responseText;
                }
                 else if(type == 10)
                {
                  document.getElementById("treatment-description").innerHTML=XMLHttpRequestObject.responseText;
                }

                else if(type == 11)
                {
                  document.getElementById("anchorage-description").innerHTML=XMLHttpRequestObject.responseText;
                }
                else if(type == 12)
                {
                  document.getElementById("appliance-description").innerHTML=XMLHttpRequestObject.responseText;
                }

             }
         }
                 
         XMLHttpRequestObject.send(null);
     }


   }

    function save_doctors_notes(visit_id,patient_id,type)
   {

      var config_url = $('#config_url').val();
      var data_url = "<?php echo site_url();?>dental/save_soap_notes/"+visit_id+"/"+type+"/"+patient_id;
      

      if(type == 1)
      {
         var symptoms = document.getElementById("facial_asymmetry_description"+visit_id).value; 
      }
      else if(type == 2)
      {
         var symptoms = document.getElementById("intra_oral_description"+visit_id).value; 
      }
      else if(type == 3)
      {
        var symptoms = document.getElementById("crossibet_description"+visit_id).value;
      }
      else if(type == 4)
      {
        var symptoms = document.getElementById("opg_description"+visit_id).value;
      }
      else if(type == 5)
      {
        var symptoms = document.getElementById("ceph_description"+visit_id).value;
      }
      else if(type == 6)
      {
        var symptoms = document.getElementById("pa_description"+visit_id).value;
      }
      else if(type == 7)
      {
        var symptoms = document.getElementById("bw_description"+visit_id).value;
      }
      else if(type == 8)
      {
        var symptoms = document.getElementById("diagnosis_description"+visit_id).value;
      }
      else if(type == 9)
      {
        var symptoms = document.getElementById("problem_description"+visit_id).value;
      }
      else if(type == 10)
      {
        var symptoms = document.getElementById("treatment_description"+visit_id).value;
      }
      else if(type == 11)
      {
        var symptoms = document.getElementById("anchorage_description"+visit_id).value;
      }
      else if(type == 12)
      {
        var symptoms = document.getElementById("appliance_description"+visit_id).value;
      }
      
      

      // window.alert(symptoms);

      $.ajax({
        type:'POST',
        url: data_url,
        data:{notes: symptoms},
        dataType: 'json',
        success:function(data){
          if(data.result == 'success')
          {
            // $('#history_notes').html(data.message);
            // tinymce.get('visit_presenting_complaint').setContent('');
           
            alert("You have successfully saved the note");
            
            display_notes(visit_id,patient_id,type);
          }
          else
          {
            alert("Unable to add the action plan");
          }
        //obj.innerHTML = XMLHttpRequestObject.responseText;
        },
        error: function(xhr, status, error) {
        //alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);
        alert(error);
        }
      });

   }

    function get_oral_teeth(page_id,patient_id,chart_type = null)
    {
        // alert(page_id);
        // get_page_links(page_id,patient_id);
        // if(page_id > 1)
        // {
            var visit_id = document.getElementById("visit_id").value;//window.localStorage.getItem('visit_id');
             // alert(visit_id);
            

              var visit_id = visit_id;
              // get_page_header(visit_id); 
              var url = "<?php echo base_url();?>dental/get_oral_teeth_view/"+page_id+"/"+patient_id+"/"+visit_id+"/"+chart_type;  
              // alert(url);
              $.ajax({
              type:'POST',
              url: url,
              data:{page_id: page_id,patient_id: patient_id},
              dataType: 'text',
              success:function(data){
                  var data = jQuery.parseJSON(data);
                  var page_item = data.page_item;

                  // alert(page_item);
                  $('#oral-teeth').html(data.page_item);
              },
              error: function(xhr, status, error) {
              // alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);
                  // display_patient_bill(visit_id);
              }
              });                
            
       // return false;

    }

    function save_missing_teeth(tooth_id)
    {
    	// var tooth_id = document.getElementById("tooth_id").value;
	     var visit_id = document.getElementById("visit_id").value;
	     var patient_id = document.getElementById("patient_id").value;


	    // var radios = 7;
	    var cavity_status = 7;
	   
	    
	     

	     var url = "<?php echo base_url();?>dental/save_missing_teeth/"+visit_id+"/"+patient_id;
	     //
	     $.ajax({
	     type:'POST',
	     url: url,
	     data:{tooth_id: tooth_id,patient_id: patient_id,cavity_status: cavity_status},
	     dataType: 'text',
	     success:function(data){
	       // var prescription_view = document.getElementById("prescription_view");
	       // prescription_view.style.display = 'none';

	         var data = jQuery.parseJSON(data);
	            
	            var status = data.status;

	            if(status == 'success')
	            {
	              alert(data.message);
	              get_oral_teeth(1,patient_id);
	              // display_patient_history(visit_id,patient_id);

	            }
	            else
	            {
	              alert(data.message);
	            }
	     
	     },
	     error: function(xhr, status, error) {
	     alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);
	     
	     }
	     });
	     
	     return false;
    }




    function get_rotation_teeth(page_id,patient_id,chart_type = null)
    {
        // alert(page_id);
        // get_page_links(page_id,patient_id);
        // if(page_id > 1)
        // {
            var visit_id = document.getElementById("visit_id").value;//window.localStorage.getItem('visit_id');
             // alert(visit_id);
            

              var visit_id = visit_id;
              // get_page_header(visit_id); 
              var url = "<?php echo base_url();?>dental/get_rotation_teeth_view/"+page_id+"/"+patient_id+"/"+visit_id+"/"+chart_type;  
              // alert(url);
              $.ajax({
              type:'POST',
              url: url,
              data:{page_id: page_id,patient_id: patient_id},
              dataType: 'text',
              success:function(data){
                  var data = jQuery.parseJSON(data);
                  var page_item = data.page_item;

                  // alert(page_item);
                  $('#rotation-teeth').html(data.page_item);
              },
              error: function(xhr, status, error) {
              // alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);
                  // display_patient_bill(visit_id);
              }
              });                
            
       // return false;

    }


    function get_service_charges(service_id,visit_id)
	{
		 
		 var config_url = $('#config_url').val();
		
		 $('#view-service-items').css('display', 'block');
		
			
		 window.localStorage.setItem('service_id',service_id);
		 window.localStorage.setItem('visit_id',visit_id);
          // get_page_header(visit_id); 
          var url = config_url+"dental/get_all_service_charges/"+service_id+"/"+visit_id;  
          // alert(url);
          $.ajax({
          type:'POST',
          url: url,
          data:{query: null},
          dataType: 'text',
          success:function(data){
              var data = jQuery.parseJSON(data);
              // alert(page_item);
              $('#all-service-charges').html(data.result);
          },
          error: function(xhr, status, error) {
          alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);
              // display_patient_bill(visit_id);
          }
          });
	}

    	function service_list()
	{
		 
		 var config_url = $('#config_url').val();
		 var query = $('#service_charge_name').val();

		 var service_id = window.localStorage.getItem('service_id');
		 var visit_id = window.localStorage.getItem('visit_id');
          // get_page_header(visit_id); 
          var url = config_url+"dental/get_all_service_charges/"+service_id+"/"+visit_id;  
          // alert(url);
          $.ajax({
          type:'POST',
          url: url,
          data:{query: query},
          dataType: 'text',
          success:function(data){
              var data = jQuery.parseJSON(data);
              // alert(page_item);
              $('#all-service-charges').html(data.result);
          },
          error: function(xhr, status, error) {
          alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);
              // display_patient_bill(visit_id);
          }
          });
	}

    function save_rotation_teeth(tooth_id)
    {
    	// var tooth_id = document.getElementById("tooth_id").value;
	     var visit_id = document.getElementById("visit_id").value;
	     var patient_id = document.getElementById("patient_id").value;


	    // var radios = 7;
	    var cavity_status = 7;
	   
	    
	     

	     var url = "<?php echo base_url();?>dental/save_rotation_teeth/"+visit_id+"/"+patient_id;
	     //
	     $.ajax({
	     type:'POST',
	     url: url,
	     data:{tooth_id: tooth_id,patient_id: patient_id,cavity_status: cavity_status},
	     dataType: 'text',
	     success:function(data){
	       // var prescription_view = document.getElementById("prescription_view");
	       // prescription_view.style.display = 'none';

	         var data = jQuery.parseJSON(data);
	            
	            var status = data.status;

	            if(status == 'success')
	            {
	              alert(data.message);
	              get_rotation_teeth(1,patient_id);
	              // display_patient_history(visit_id,patient_id);

	            }
	            else
	            {
	              alert(data.message);
	            }
	     
	     },
	     error: function(xhr, status, error) {
	     alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);
	     
	     }
	     });
	     
	     return false;
    }
</script>