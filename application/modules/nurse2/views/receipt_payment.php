<?php
$patient = $this->reception_model->patient_names2(NULL, $visit_id);
$account_balance = $patient['account_balance'];
$total_waiver1 = $this->accounts_model->get_visit_waiver($visit_id);
?>


 <section class="panel ">
	
	<!-- Widget content -->
	
	<div class="panel-body">
		<div class="row">
			<div class="col-md-12">
			<?php
				$error = $this->session->userdata('error_message');
				$success = $this->session->userdata('success_message');
				
				if(!empty($error))
				{
				  echo '<div class="alert alert-danger">'.$error.'</div>';
				  $this->session->unset_userdata('error_message');
				}
				
				if(!empty($success))
				{
				  echo '<div class="alert alert-success">'.$success.'</div>';
				  $this->session->unset_userdata('success_message');
				}
			 ?>
			</div>
		</div>
<div class="row">
			<div class="col-md-12">
			
				<div class="col-md-5">
							<section class="panel panel-featured panel-featured-info">
								<header class="panel-heading">
									<h2 class="panel-title">Receipts</h2>
								</header>
								
								<div class="panel-body">
                                		<?php echo form_open("accounts/make_payment_charge/".$visit_id., array("class" => "form-horizontal"));?>
										<div class="form-group">
											<div class="col-lg-6">
                                            	<div class="radio">
                                                    <label>
                                                        <input id="optionsRadios2" type="radio" name="type_payment" value="1" checked="checked" onclick="getservices(1)"> 
                                                        Normal
                                                    </label>
                                                </div>
											</div>
											<div class="col-lg-6">
                                            	<div class="radio">
                                                    <label>
                                                        <input id="optionsRadios2" type="radio" name="type_payment" value="2" onclick="getservices(2)"> 
                                                        Waiver
                                                    </label>
                                                </div>
											</div>
											<div class="col-lg-4">
                                            	<div class="radio">
                                                    <label>
                                                        <input id="optionsRadios2" type="radio" name="type_payment" value="3" onclick="getservices(3)"> 
                                                        Credit Note
                                                    </label>
                                                </div>
											</div>
										</div>
                                        <input type="hidden" name="service_id" value="0">
										<div id="service_div2" class="form-group" style="display:none;">
											<!-- <label class="col-lg-4 control-label">Service: </label>
										  
											<div class="col-lg-8">
												
                                            	<select name="service_id" class="form-control" >
                                                	<option value="">All services</option>
                                            	<?php
												if(count($item_invoiced_rs) > 0)
												{
													$s=0;
													foreach ($item_invoiced_rs as $key_items):
														$s++;
														$service_id = $key_items->service_id;
														$service_name = $key_items->service_name;
														?>
                                                        <option value="<?php echo $service_id;?>"><?php echo $service_name;?></option>
														<?php
													endforeach;
												}
													
												//display DN & CN services
												if(count($payments_rs) > 0)
												{
													foreach ($payments_rs as $key_items):
														$payment_type = $key_items->payment_type;
														
														if(($payment_type == 2) || ($payment_type == 3))
														{
															$payment_service_id = $key_items->payment_service_id;
															
															if($payment_service_id > 0)
															{
																$service_associate = $this->accounts_model->get_service_detail($payment_service_id);
																?>
																<option value="<?php echo $payment_service_id;?>"><?php echo $service_associate;?></option>
																<?php
															}
														}
														
													endforeach;
												}
												?>
                                                </select>
											</div> -->
										</div>
                                        
                                    	<div id="service_div" class="form-group" style="display:none;">
                                            <!-- <label class="col-lg-4 control-label"> Services: </label>
                                            
                                            <div class="col-lg-8">
                                                <select class="form-control" name="payment_service_id" >
                                                	<option value="">--Select a service--</option>
													<?php
                                                    $service_rs = $this->accounts_model->get_all_service();
                                                    $service_num_rows = count($service_rs);
                                                    if($service_num_rows > 0)
                                                    {
														foreach($service_rs as $service_res)
														{
															$service_id = $service_res->service_id;
															$service_name = $service_res->service_name;
															

																echo '<option value="'.$service_id.'">'.$service_name.'</option>';
															
															
														}
                                                    }
                                                    ?>
                                                </select>
                                            </div> -->

                                     <div class="form-group">
										<label class="col-lg-4 control-label">Amount: </label>

										<div class="col-lg-8">
											<input type="text" class="form-control" name="waiver_amount" placeholder="" autocomplete="off">
										</div>
									</div>

									<div class="form-group">
										<label class="col-lg-4 control-label">Reason: </label>

										<div class="col-lg-8">
											<textarea class="form-control" name="reason" placeholder="" autocomplete="off"></textarea>
										</div>
									</div>
                                        </div>
                                         <input type="hidden" class="form-control" name="redirect_url" placeholder="" autocomplete="off" value="<?php echo $this->uri->uri_string()?>">
										<div class="form-group">
											<label class="col-lg-2 control-label">Amount: </label>
										  
											<div class="col-lg-10">
												<input type="text" class="form-control" name="amount_paid" placeholder="" autocomplete="off">
											</div>
										</div>
										
										<div class="form-group" id="payment_method">
											<label class="col-lg-2 control-label">Payment Method: </label>
											  
											<div class="col-lg-10">
												<select class="form-control" name="payment_method" onchange="check_payment_type(this.value)">
                                                	<?php
													  $method_rs = $this->accounts_model->get_payment_methods();
													  $num_rows = count($method_rs);
													 if($num_rows > 0)
													  {
														
														foreach($method_rs as $res)
														{
														  $payment_method_id = $res->payment_method_id;
														  $payment_method = $res->payment_method;
														  
															echo '<option value="'.$payment_method_id.'">'.$payment_method.'</option>';
														  
														}
													  }
												  ?>
												</select>
											  </div>
										</div>
										<div id="mpesa_div" class="form-group" style="display:none;" >
											<label class="col-lg-2 control-label"> Mpesa TX Code: </label>

											<div class="col-lg-8">
												<input type="text" class="form-control" name="mpesa_code" placeholder="">
											</div>
										</div>
									  
										<div id="insuarance_div" class="form-group" style="display:none;" >
											<label class="col-lg-2 control-label"> Insurance Number: </label>
											<div class="col-lg-10">
												<input type="text" class="form-control" name="insuarance_number" placeholder="">
											</div>
										</div>
									  
										<div id="cheque_div" class="form-group" style="display:none;" >
											<label class="col-lg-2 control-label"> Cheque Number: </label>
										  
											<div class="col-lg-10">
												<input type="text" class="form-control" name="cheque_number" placeholder="">
											</div>
										</div>
									  
								<!-- 		<div id="username_div" class="form-group" style="display:none;" >
											<label class="col-lg-2 control-label"> Username: </label>
										  
											<div class="col-lg-10">
												<input type="text" class="form-control" name="username" placeholder="">
											</div>
										</div>
									 
										<div id="password_div" class="form-group" style="display:none;" >
											<div class="form-group">
												<label class="col-lg-2 control-label"> Password: </label>										  
												<div class="col-lg-10">
													<input type="password" class="form-control" name="password" placeholder="">
												</div>
											</div>
											<div class="form-group">
							                    <label class="col-md-2 control-label">Description: </label>
							                    
							                    <div class="col-md-10">
							                        <textarea class="form-control" name="waiver_description"></textarea>
							                    </div>
							                </div>

										</div> -->
										 

										<div class="center-align">
											<button class="btn btn-info btn-sm" type="submit" onclick="return confirm('Are you sure you want to create this receipt ? ')">Add Payment Information</button>
										</div>
										
										<?php echo form_close();?>
										<br>
									
								</div>
							</section>
						</div>
				<!-- END OF THE SPAN 7 -->
			</div>
		</div>
		<div class="row">
	    	<div class="col-md-12 center-align">
	    		<h4><strong> WAIVER. <?php echo number_format($total_waiver1,2)?></strong></h4>
	        </div>
	    </div>
		

	
	</div>
</section>
  <!-- END OF ROW -->
<script type="text/javascript">

 
   $(function() {
       $("#service_id_item").customselect();
       $("#provider_id_item").customselect();
       $("#parent_service_id").customselect();

   });
   $(document).ready(function(){
   		// display_patient_bill(<?php echo $visit_id;?>);
   });
     
  function getservices(id){

        var myTarget1 = document.getElementById("service_div");
        var myTarget2 = document.getElementById("username_div");
        var myTarget3 = document.getElementById("password_div");
        var myTarget4 = document.getElementById("service_div2");
        var myTarget5 = document.getElementById("payment_method");
		
        if(id == 1)
        {
          myTarget1.style.display = 'none';
          myTarget2.style.display = 'none';
          myTarget3.style.display = 'none';
          myTarget4.style.display = 'block';
          myTarget5.style.display = 'block';
        }
        else
        {
          myTarget1.style.display = 'block';
          myTarget2.style.display = 'block';
          myTarget3.style.display = 'block';
          myTarget4.style.display = 'none';
          myTarget5.style.display = 'none';
        }
        
  }
  function check_payment_type(payment_type_id){
   
    var myTarget1 = document.getElementById("cheque_div");

    var myTarget2 = document.getElementById("mpesa_div");

    var myTarget3 = document.getElementById("insuarance_div");

    if(payment_type_id == 1)
    {
      // this is a check
     
      myTarget1.style.display = 'block';
      myTarget2.style.display = 'none';
      myTarget3.style.display = 'none';
    }
    else if(payment_type_id == 2)
    {
      myTarget1.style.display = 'none';
      myTarget2.style.display = 'none';
      myTarget3.style.display = 'none';
    }
    else if(payment_type_id == 3)
    {
      myTarget1.style.display = 'none';
      myTarget2.style.display = 'none';
      myTarget3.style.display = 'block';
    }
    else if(payment_type_id == 4)
    {
      myTarget1.style.display = 'none';
      myTarget2.style.display = 'none';
      myTarget3.style.display = 'none';
    }
    else if(payment_type_id == 5)
    {
      myTarget1.style.display = 'none';
      myTarget2.style.display = 'block';
      myTarget3.style.display = 'none';
    }
    else
    {
      myTarget2.style.display = 'none';
      myTarget3.style.display = 'block';  
    }

  }

   function display_patient_bill(visit_id){

      var XMLHttpRequestObject = false;
          
      if (window.XMLHttpRequest) {
      
          XMLHttpRequestObject = new XMLHttpRequest();
      } 
          
      else if (window.ActiveXObject) {
          XMLHttpRequestObject = new ActiveXObject("Microsoft.XMLHTTP");
      }
      
      var config_url = document.getElementById("config_url").value;
      var url = config_url+"accounts/view_patient_bill/"+visit_id;
      // alert(url);
      if(XMLHttpRequestObject) {
                  
          XMLHttpRequestObject.open("GET", url);
                  
          XMLHttpRequestObject.onreadystatechange = function(){
              
              if (XMLHttpRequestObject.readyState == 4 && XMLHttpRequestObject.status == 200) {

                  document.getElementById("patient_bill").innerHTML=XMLHttpRequestObject.responseText;
              }
          }
                  
          XMLHttpRequestObject.send(null);
      }
  }

  	function remove_charged_item(visit_charge_id,visit_id)
	{

		// var visit_charge_id = $('#v_procedure_id').val();

		// alert(visit_charge_id);	
		var remove_description = $('#remove_description'+visit_charge_id).val();
		// alert(remove_description);	
		// var visit_id = $('#visit_to_charge').val();
		var url = "<?php echo base_url();?>accounts/cancel_charge/"+visit_charge_id+"/"+visit_id;	

		// alert(url);	
		$.ajax({
		type:'POST',
		url: url,
		data:{remove_description: remove_description},
		dataType: 'json',
		success:function(data){
			
			alert(data.message);
			if(data.status == "success")
			{
				$('#remove_charge'+visit_charge_id).modal('toggle');
		 		display_patient_bill(visit_id);
			}
		 	
		},
		error: function(xhr, status, error) {
		// alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);
			display_patient_bill(visit_id);
		}
		});
		// return false;
	};

	// //Calculate procedure total
	// function calculatetotal(amount, id, procedure_id, v_id){
	       
	//     var units = document.getElementById('units'+id).value;  
	//     var billed_amount = document.getElementById('billed_amount'+id).value;  

	//     grand_total(id, units, billed_amount, v_id);
	// }
	function grand_total(procedure_id, units, amount, v_id){
	    var XMLHttpRequestObject = false;
	        
	    if (window.XMLHttpRequest) {
	    
	        XMLHttpRequestObject = new XMLHttpRequest();
	    } 
	        
	    else if (window.ActiveXObject) {
	        XMLHttpRequestObject = new ActiveXObject("Microsoft.XMLHTTP");
	    }
	    var config_url = document.getElementById("config_url").value;
	    var url = config_url+"accounts/update_service_total/"+procedure_id+"/"+units+"/"+amount;
	    // alert(url);
	    if(XMLHttpRequestObject) {
	                
	        XMLHttpRequestObject.open("GET", url);
	                
	        XMLHttpRequestObject.onreadystatechange = function(){
	            
	            if (XMLHttpRequestObject.readyState == 4 && XMLHttpRequestObject.status == 200) 
				{
	    			display_patient_bill(v_id);
	            }
	        }
	                
	        XMLHttpRequestObject.send(null);
	    }
	}
	function delete_service(id, visit_id){
	    var XMLHttpRequestObject = false;
	        
	    if (window.XMLHttpRequest) {
	    
	        XMLHttpRequestObject = new XMLHttpRequest();
	    } 
	        
	    else if (window.ActiveXObject) {
	        XMLHttpRequestObject = new ActiveXObject("Microsoft.XMLHTTP");
	    }
	     var config_url = document.getElementById("config_url").value;
	    var url = config_url+"accounts/delete_service_billed/"+id;
	    
	    if(XMLHttpRequestObject) {
	                
	        XMLHttpRequestObject.open("GET", url);
	                
	        XMLHttpRequestObject.onreadystatechange = function(){
	            
	            if (XMLHttpRequestObject.readyState == 4 && XMLHttpRequestObject.status == 200) {

	                display_patient_bill(visit_id);
	            }
	        }
	                
	        XMLHttpRequestObject.send(null);
	    }
	}
	function save_service_items(visit_id)
	{
		var provider_id = $('#provider_id'+visit_id).val();
		var service_id = $('#service_id'+visit_id).val();
		var visit_date = $('#visit_date_date'+visit_id).val();
		var url = "<?php echo base_url();?>accounts/add_patient_bill/"+visit_id;
		
		$.ajax({
		type:'POST',
		url: url,
		data:{provider_id: provider_id, service_charge_id: service_id, visit_date: visit_date},
		dataType: 'text',
		success:function(data){
			alert("You have successfully billed");
			display_patient_bill(visit_id);
		},
		error: function(xhr, status, error) {
		alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);
			display_patient_bill(visit_id);
		}
		});
		return false;
	}

		function calculatetotal(amount, id, procedure_id, v_id){
	       
	    var units = document.getElementById('units'+id).value;  
	    var billed_amount = document.getElementById('billed_amount'+id).value;  

	    grand_total(id, units, billed_amount, v_id);
	}
	function reverse_charged_value(id, visit_id){

		var res = confirm('Are you sure you want to reverse this charge?');
     
	    if(res)
	    {

	    	var config_url = document.getElementById("config_url").value;
	    	var url = config_url+"accounts/reverse_service_billed/"+id+"/"+visit_id;
		
			$.ajax({
			type:'POST',
			url: url,
			data:{visit_id: visit_id,id: id},
			dataType: 'json',
			success:function(data){
				alert(data.message);
				display_patient_bill(visit_id);
			},
			error: function(xhr, status, error) {
			alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);
				display_patient_bill(visit_id);
			}
			});
			return false;
		 }
	}

 
</script>
